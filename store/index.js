export  const  state  = () => ({
    darkMode: false
});
export  const  mutations  = {
  TOGGLE_THEME: state => {
      state.darkMode = !state.darkMode
  }
};
export  const  getters  = {
    themeStatus: state => {
      return state.darkMode
    }
}