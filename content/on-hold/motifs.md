---
title: Motifs
longtitle: Motifs et autres pavages de clones
slug: motifs
path: cours5
thumb: motif.jpg
active: false
disabled: true
ranking: 5
lang: fr-FR
module: vectoriel
logiciel: Inkscape
extrait: Utiliser des motifs Lorem, ipsum dolor sit amet consectetur adipisicing elit. Illum ipsum saepe temporibus voluptatum expedita natus reprehenderit eius dolor veritatis! Dolorum exercitationem nostrum itaque doloribus ut tempora similique velit neque! 
content:
    - Motifs
mediasgrid: 1
medias:
    - path: delvoye1.jpg
      absolute: false  
      caption: Wim Delvoye
      link: 
      label:
    - path: vuitton.png
      absolute: false
      caption: Motif Vuitton
    - path: murakami.jpg
      absolute: false
      caption: Takashi Murakami
    - path: photo1.png
      absolute: false
      caption: 
    - path: photo2.png
      absolute: false
      caption:
    - path: photo3.png
      absolute: false
      caption: 
exercice: 
    label: 
    thumb: 
---

#### Ce que nous avons vu

+ Inkscape **interface** de base et principe du dessin vectoriel
+ Dessin de **formes simples**
+ **Contour et remplissage** / Tracés et aspects des tracés
+ **Import d'images** Bitmap
+ Outils de **magnétisme**, grille, **alignements** (panneau)
+ **Grouper/dégrouper**
+ (Base du) Dessin à la **plume** (tutoriels)
+ **Opérations booléennes** (union, différence, intersection, etc..)
+ Créer une **découpe** (créer un masque)

#### Aujourd'hui

+ Rappel Carto (pas de délai)
+ Rappel opérations booléennes + tracé transparent + Retour sur Paul Rand
+ **Clones** et **pavages de clones**
+ **Motifs** simples
+ Les **interpolations**
+ Les **dégradés** de couleur
+ **Sauvegarde** de votre fichier
+ Retour sur Mullican

<a data-fancybox title="" href="/assets/motif10.png">![](/assets/motif10.png)</a>

*Marble floor* (1999) de Wim Delvoye

<a data-fancybox title="" href="/assets/delvoye1.jpg">![](/assets/delvoye1.jpg)</a>
<a data-fancybox title="" href="/assets/delvoye2.jpg">![](/assets/delvoye2.jpg)</a>

----

### Vuittami

A l'instar de Takashi Murakami, créez votre version personnelle du motif Louis Vuitton, à partir du document ci-dessous.

Dans les deux cas, il vous faudra être méthodiques et précis (Alignements, groupes, pathfinder, etc..)
Veillez à sauver régulièrement votre travail.

-----

### Mise au net / picto

Au final, votre image ne doit comporter que 3 objets:

+ L'appareil photo
+ Le cercle blanc
+ Le fond jaune

<a data-fancybox title="" href="/assets/photo1.png">![](/assets/photo1.png)</a>
<a data-fancybox title="" href="/assets/photo2.png">![](/assets/photo2.png)</a>
<a data-fancybox title="" href="/assets/photo3.png">![](/assets/photo3.png)</a>

---- 

+ [Exercice alignements](/assets/alignements.pdf)

<a data-fancybox title="" href="/assets/dominos.png">![](/assets/dominos.png)</a>