---
title: Une carte postale pour le futur
date: 
slug: cartepostale
disabled: true
thumb: carte-thumb.jpg
ranking: 11
module: bitmap
artistique: true
logiciel: Krita
extrait: Une cartographie imaginaire, subjective, personnelle, politique, sociale, émotionnelle, écologique… Cette page reprend quelques ressources et informations supplémentaires. Vous trouverez le briefing complet sur Digitalab.be
mediasgrid: 2
medias:
    - path: 
      absolute: true
      caption: 
      link: 
      label: 
gallery:
    - path: chatonsky01.jpeg
      absolute:
      caption:
    - path: chatonsky03.jpeg
      absolute:
      caption:
    - path: chatonsky04.jpeg
      absolute:
      caption: 
exercice: 
    slug: mullican
    label: Matt Mullican
    thumb: 
    rank: 1
    horscours: true
---



Vous trouverez ci-dessous quelques documents, que nous évoquerons au cours, en rapport avec le travail *Une carte postale pour le futur*.

Pour rappel, vous trouverez le briefing de ce travail avec ses quelques contraintes techniques sur [Digitalab.be](http://www.digitalab.be/briefing-cartographie-imaginaire-2018-2019/)



 <Gallery :imggallery="gallery" :imgsection="module" :imgcours="slug"></Gallery>

Cette série d’images a été réalisée à partir de photographies d’archives de la ville d’Aubervilliers qui ont été étendues grâce à Dall-E. En modifiant et en remplaçant des éléments déjà présents (inpaint) et en augmentant la photographie sur ses bords (outpaint), le résultat est une version contrefactuelle du document d’origine.

Ainsi, dans le contexte de l’induction statistique, le statut de l’archive est modifié. Elle n’est plus la trace d’un passé, mais l’origine d’une autre possibilité, d’une variation qui dévie de la causalité historique. Cette contrefactualité modifie le statut de l’historicité et constitue une réponse paradoxale à l’accumulation des documents et à l’hypermnésie provoquées par la mondialisation du Web.
Cette série réalisée dans différents lieux (Ostabat, Cité des Sciences et de l’Industrie) fait partie d’un projet plus large nommé Complétion, consistant à explorer la modification des archives par l’IA.

<figure>
  <img src="/cours/bitmap/cartepostale/cartes.jpg" alt="">
  <figcaption>
    <span>Quelques travaux d'étudiant.e.s</span>
      <div class="img-actions">
        <a href="https://galerieb1.netlify.app/" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" height="20" width="20" fill="#ff1e00"><path d="M4.625 17.25q-.771 0-1.323-.552-.552-.552-.552-1.323V4.625q0-.771.552-1.323.552-.552 1.323-.552h5.458v1.625H4.625q-.083 0-.167.083-.083.084-.083.167v10.75q0 .083.083.167.084.083.167.083h10.75q.083 0 .167-.083.083-.084.083-.167V9.917h1.625v5.458q0 .771-.552 1.323-.552.552-1.323.552Zm3.396-4.125-1.146-1.146 7.604-7.604h-2.083V2.75h4.854v4.854h-1.625V5.521Z"/></svg></a>
      </div>
  </figcaption>
</figure>