---
name: Sketch.app
url: https://sketch.app
slug: sketch
path: sketch
thumb: sketch.jpg
logo: sketch.png
ecole: false
webapp: false
type: propriétaire
active: true
ranking: 0
lang: fr-FR
modules: 
    - vectoriel
    - web
extrait: Sketch est un logiciel vectoriel dédié à la création de maquettes pour le web. Il est très performant et ergonomique. Il est payant mais sous la forme d'un one-shot, pas un abonnement. C'est une référence dans le domaine, comme l'est Figma (en ligne)
hascontent: false
---