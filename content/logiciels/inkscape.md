---
name: Inkscape
url: https://inkscape.org
slug: inkscape
path: inkscape
logo: inkscape-logo.png
thumb: inkscape.jpg
ecole: true
webapp: false
type: floss
active: true
ranking: 0
lang: fr-FR
modules: 
    - vectoriel
extrait: Inkscape est un logiciel d'édition vectorielle libre. C'est la principale alternative libre à Illustrator. Prenez le temps de personnaliser l'interface et d'explorer ses possibilités. C'est une application pleine de ressources..
hascontent: false
---