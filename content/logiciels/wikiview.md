---
name: Wikiview.net
url: https://wikiview.net
slug: wikiview
path: wikiview
thumb: wikiview.jpg
logo: wikiview.png
type: floss
ecole: false
webapp: true
active: true
ranking: 0
lang: fr-FR
modules: 
    - bitmap
    - web
extrait: Un extraordinaire outil de recherche d'images qui vous permet de naviguer dans les dizaines de millions d'images libres de la Wikimedia Foundation. 
hascontent: false
---