---
name: Krita
url: https://krita.org
slug: krita
path: krita
thumb: 
logo: krita.png
ecole: true
webapp: false
type: libre
active: true
ranking: 0
lang: fr-FR
modules: 
    - bitmap
extrait: Krita est un logiciel libre dédié au travail de l'image bitmap, à la peinture numérique et à l'animation.
hascontent: false
---