---
title: Images matricielles et sélections
longtitle: Images matricielles et sélections
slug: selections
date: 21/10/22
disabled: false
path: cours2
thumb: selection-thumb.png
caption: test
active: true
current: false
ranking: 7
lang: fr-FR
module: bitmap
logiciel: Krita
extrait: Découverte de l'interface de Krita, des notions de résolution, définition et profondeur d'image, ainsi que les outils de sélections de base et de transformation.
content:
    - Notions de résolution, définition et profondeur d'image
    - Outils de sélection
mediasgrid: 1
medias:
    - path: https://arweave.net/NyN-qNHgd7BLX4st4qRozYaZMLt6CEHAfaUww2c3vHE
      absolute: true
      caption: P1xel fool, TA-ZERO-09, GIF, 1920X1080, 180 FRAMES, 4 COLORS, 2021 (https://p1xelfool.com/ta-zero/)
    - path: villa-del-casale.png
      absolute: false  
      caption: Villa del Casale
    - path: andreppp.png
      absolute: false  
      caption: test
    - path: 256.jpg
      absolute: false  
      caption: test
    - path: http://cours.baptiste-tosi.eu/lib/exe/fetch.php?media=22901741961_e619efbd78_b.jpg
      absolute: true  
      caption: René Magritte
gallery:
    - path: wikiview.jpg
      absolute:
      caption: Wikiview.net
    - path: hockney.jpeg
      absolute:
      caption: David Hockney, Normandie
exercice: 
    slug: selections
    label: Sélections
    thumb: 
    rank: 1
    horscours: true
---

<iframe width="740" height="416" style="border-radius:.5rem;" src="https://www.youtube.com/embed/axTPnSd5f3M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[https://www.youtube.com/watch?time_continue=167&v=0QO0yZldC2M](https://www.youtube.com/watch?time_continue=167&v=0QO0yZldC2M)

[Place / The Atlas](https://draemm.li/various/place-atlas/)

## Intro

+ Réception des **cartographies**

## Images matricielles

+ [https://jspaint.app/](https://jspaint.app/)
+ Images matricielles (bitmap)
+ [Bits](https://fr.wikipedia.org/wiki/Octet) & maps
+ **définition, résolution, profondeur**
+ Filtres Google et images libres
+ Trouver des images de qualité. + vos images
+ [Tineye: Reverse image search](https://www.tineye.com/)
+ [Wikiview](https://wikiview.net/)
+ Formats de fichier (RAW, JPG, PNG, GIF, TIFF)
+ Drive et les fichiers bitmap dans le cadre du cours

<Gallery :imggallery="gallery" :imgsection="module" :imgcours="slug"></Gallery>

## La question des logiciels

+ [Krita](https://krita.org/fr/)
+ The [Gimp](https://www.gimp.org/)
+ Suite [Affinity](https://affinity.serif.com/fr/)
+ Adobe [Photoshop](https://www.adobe.com/be_en/products/photoshop.html)
+ [Photopea](https://www.photopea.com/)

## Krita: Interface, sélections et calques

+ Personnalisation de l'**interface et espaces de travail**
+ Les **calques** (Nouveau, dupliquer, opacité, groupes..)
+ La **couleur**, Avant-plan/Arrière plan
+ **Zoomer** dans une image bitmap
+ Principe de **non-destructivité** des images, règles de base
+ Principaux **outils de sélection**
+ La **plume** comme outil de sélection
+ **Etendre/réduire** une sélection, **inverser** une sélection, **transformer** une sélection
+ Utilisation du **pinceau**, modification des brosses (pensez au workshop)
+ [https://commons.wikimedia.org/wiki/File:Orion_Nebula_-_Hubble_2006_mosaic_18000.jpg?uselang=fr](https://commons.wikimedia.org/wiki/File:Orion_Nebula_-_Hubble_2006_mosaic_18000.jpg?uselang=fr)
+ [https://fr.wikipedia.org/wiki/Image_num%C3%A9rique#D%C3%A9finition_et_r%C3%A9solution](https://fr.wikipedia.org/wiki/Image_num%C3%A9rique#D%C3%A9finition_et_r%C3%A9solution)
+ **Masques de transparence** (ou masques de fusion dans Photoshop)

### Aussi

+ [About bitmaps: http://paulbourke.net/dataformats/bitmaps/](http://paulbourke.net/dataformats/bitmaps/)
+ [Dithering](https://www.google.com/search?q=pixel+dithering&source=lnms&tbm=isch&sa=X&ved=0ahUKEwj4l9ye8-vlAhXH2aQKHWRDAoMQ_AUIEigB&biw=1583&bih=948&dpr=2#imgrc=3atyA57EjCi8RM:)
+ [Adobe: les sélections](https://helpx.adobe.com/be_fr/photoshop/how-to/selection-tools-basics.html)
+ [http://neocha.com/magazine/hisham-akira-bharoocha/](http://neocha.com/magazine/hisham-akira-bharoocha/)
+ [Krita for Photoshop users](http://kde.avc.cx/krita/marketing/IntroductiontoKritaforusercomingfromPs.pdf)
+ [Krita docs](https://docs.krita.org/fr/general_concepts.html)

