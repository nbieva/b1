---
title: Masques de transparence
longtitle: Masques de transparence et travail non destructif
slug: masques
date: 28/10/22
disabled: false
path: masques
thumb: thumb-masques.jpg
caption: 
active: true
current: false
ranking: 8
lang: fr-FR
module: bitmap
logiciel: Krita
extrait: Le principe des masques de transparence (ou de fusion). Le masque de transparence va rendre plus ou moins visibles les pixels du calque auquel il est associé.
content:
    - Masques de transparence
mediasgrid: 1
medias:
    - path: masques.png
      absolute: false  
      caption: Masques de transparence ou de filtrage
      link: https://kanghee.kim/
    - path: kanghee01.png
      absolute: false  
      caption: Kanghee Kim
      link: https://kanghee.kim/
    - path: kanghee02.png
      absolute: false  
      caption: Kanghee Kim
      link: https://kanghee.kim/
    - path: kanghee03.png
      absolute: false  
      caption: Kanghee Kim
      link: https://kanghee.kim/

exercice: 
    slug: masques
    label: masques
    thumb: 
    rank: 1
    horscours: true
---

# Les masques de transparence

+ Retour sur les **sélections**
+ Mode **interface cachée** (Tab)
+ **Rotation de l'espace de travail** (+ raccourci Maj+Space)
+ **Superposition** d'images + Modes de fusion
+ [Les préréglages de brosses](https://docs.krita.org/en/reference_manual/krita_4_preset_bundle.html)
+ Modifier la taille de la brosse à la volée: Shift + Clic + Aller vers la droite ou vers la gauche
+ Pipette rapide : Cmd + Clic
+ Les **masques de transparence** : principes de base (et un oeil dans d'autres logiciels)
+ Création de masque avec sélection active
+ **Duplication** de masques
+ Préparer un calque pour la gestion de la transparence
+ Calque de **filtrages** : Réglages **Désaturer** et **Niveaux**
+ **Aplatir** un calque
+ **Convertir** un calque en masque de transparence
+ **Eclaircir** ou **assombrir** les tons clairs/moyens/foncés / Outils Densité- et densité+
+ **Isoler** le calque

## Digital collage

Composez une image de 2000 x 2000px

La matière première de votre image doit être un minimum 10 autres images. Attention à toujours garder un oeil sur la définition de vos images sources.

+ Jouez sur l'échelle
+ Songez à rester (en fonction du résultat souhaité) dans une même gamme de couleur et avec une luminosité similaire (en focntion de vos combinaisons)
+ Vous pouvez également expérimenter les différentes brosses par défaut de Krita.
+ Songez à toutes les images et sujets qui sont particulièrement adaptés au principe des masques (que vous pouvez facilement transformer en image en niveaux de gris)

**ATTENTION: Cette fois, travail non-destructif de l'image!** (D'où l'utilisation des masques..)
Et veillez à la bonne organisation de vos calques ainsi qu’à la qualité des images utilisées! La recherche d'images adaptées et de qualité est une des étapes les plus importantes de ce travail.
**Sauvez régulièrement votre travail!**
