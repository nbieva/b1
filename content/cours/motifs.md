---
title: Motifs, clones et pavages
date: 14/10/22
longtitle: patterns
slug: motifs
path: motifs
thumb: motif.jpg
active: false
ranking: 4
lang: fr-FR
module: vectoriel
logiciel: Inkscape
extrait: Les clones, pavages et pavages de clones dans Inkscape. De magnifiques outils de recherche pour la création de motifs et de textures mais aussi pour la recherche de formes..
content:
    - Masques
    - Combinaisons de formes
mediasgrid: 1
medias:
    - path: https://www.goodman-gallery.com/artists/22653/file_image
      absolute: true  
      caption: Kendell Geers, Be/LIE/VE, 2006, Mural (Dimensions Variables)
    - path: https://extracitykunsthal.be/img/uploads/media/phaidon-thomas-bayrle-800x800.jpg
      absolute: true  
      caption: Thomas Bayrle
    - path: 10print.jpg
      absolute:
      caption: 10 print en langage Basic
    - path: https://leonpolksmithfoundation.org/wp-content/uploads/2018/12/lps-homepage-art.jpg
      absolute: true  
      caption: Leon Polk Smith, Constellation Twelve Circles, 1969
    - path: https://pbs.twimg.com/media/FML6IZ0WUAIXA2g.jpg
      absolute: true  
      caption: Chris Ware, Pedantry & Pedagogy
      link: https://pbs.twimg.com/media/FML6IZ0WUAIXA2g.jpg
      label: Chris Ware
    - path: projekt.jpg
      absolute: false  
      caption: Projekt 127, 6, 1978 - Hubert Hilscher
      link: https://www.designreviewed.com/artefacts/projekt-127-6-1978/
      label: Projekt 127
    - path: cle.jpg
      absolute: false
      caption: AIA NY Center for Architecture arts and culture, USA (Pentagram, 2001)
    - path: cle-calques.jpg
      absolute: false
      caption: 

gallery:
    - path: truchet0.jpg
      absolute:
      caption: Sébastien Truchet - Méthode pour faire une infinité de desseins différens, avec des carreaux mi-partis de deux couleurs par une ligne diagonale, ou Observations du P. Dominique Douat, chez Florentin de Laulne, Paris, 1722
    - path: truchet1.jpg
      absolute:
      caption: Pavages de Truchet
portraits:
    - path: eva.svg
      absolute: false
      caption: Eva
    - path: neige.svg
      absolute: false
      caption: Neige
    - path: teo.svg
      absolute: false
      caption: Téo
    - path: amandine.svg
      absolute: false
      caption: Amandine
    - path: zoe.svg
      absolute: false
      caption: Zoé
    - path: isabelle.svg
      absolute: false
      caption: Isabelle
    - path: camille1.svg
      absolute: false
      caption: Camille
    - path: camille2.svg
      absolute: false
      caption: Camille
    - path: junior.svg
      absolute: false
      caption: Junior
    - path: louise.svg
      absolute: false
      caption: Louise
    - path: clementine.svg
      absolute: false
      caption: Clémentine
    - path: candice.svg
      absolute: false
      caption: Candice
    - path: alban-di.svg
      absolute: false
      caption: Alban
    - path: ambre-di.svg
      absolute: false
      caption: Ambre
    - path: anna-bella-di.svg
      absolute: false
      caption: Anna-Bella
    - path: ines-di.svg
      absolute: false
      caption: Inès
    - path: louis-di.svg
      absolute: false
      caption: Louis
    - path: martin-di.jpg
      absolute: false
      caption: Martin
    - path: mathias-di.svg
      absolute: false
      caption: Mathias
    - path: theodore-di.svg
      absolute: false
      caption: Théodore
exercice: 
    slug: paul-rand
    label: Paul Rand
    thumb: 
    rank: 1
    horscours: true
---

+ **Opérations sur les chemins**
+ **Découpes** (masques)
+ Le **texte** dans Inkscape (texte curviligne et texte captif)
+ Exports **PDF**
+ Les **dégradés de couleurs**

<Gallery :imggallery="gallery" :imgsection="module" :imgcours="slug"></Gallery>

### Quelques liens:

- [Méthode pour faire une infinité de desseins différens, avec des carreaux mi-partis de deux couleurs par une ligne diagonale](https://gallica.bnf.fr/ark:/12148/bpt6k5823204r.texteImage)
- [Les pavages de Truchet](https://pelletierauger.com/fr/projets/les-pavages-de-truchet.html)
- [10 PRINT](https://10print.org/)
- [Pierre-Yves Desaive: The production units of Thomas Bayrle](https://www.wiels.org/fr/events/pierre-yves-desaive-les-unit%C3%A9s-de-production-de-thomas-bayrle)
- [Material Stable Diffusion model](https://replicate.com/tommoore515/material_stable_diffusion)
- [Thomas Bayrle](https://www.google.com/search?q=thomas+bayrle&rlz=1C5CHFA_enBE999BE999&source=lnms&tbm=isch&sa=X&ved=2ahUKEwir1uKqrsL6AhUSaBoKHWVvAZ4Q_AUoAXoECBgQAw&biw=1758&bih=1531&dpr=2)
- [Josh Brill](https://www.google.be/search?q=josh+brill&tbm=isch&tbo=u&source=univ&sa=X&ved=2ahUKEwjAxsnH5eLdAhXEbFAKHfVMDpsQsAR6BAgGEAE&biw=2093&bih=1043#imgrc=Iopz45zk-aw1IM:)
- [Chris Ware](https://www.centrepompidou.fr/fr/programme/agenda/evenement/m4cuua1)
- [Birds](https://designreviewed.com/artefacts/projekt-127-6-1978/?fbclid=IwAR12SrPJANXDWs4QHrXUmmUQ9dHPOiUVg0KujgdexSAM_TPrI0Xq88zTQ1U)


### Pavages

<iframe style="border-radius:8px;margin:1.5rem 0;" width="740" height="450" src="https://www.youtube.com/embed/z1O4VPxrHnI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Exporter au format SVG à partir de P5js

Solution pratique et efficace pour exporter votre sketch au format SVG.
Ressources originales (merci à eux!) :

- Article très complet de Gorilla Sun: [https://gorillasun.de/blog/working-with-svgs-in-p5js](https://gorillasun.de/blog/working-with-svgs-in-p5js)
- Zenozeng's SVG runtime for P5: [https://github.com/zenozeng/p5.js-svg](https://github.com/zenozeng/p5.js-svg)

<iframe class="video" width="740" height="460" src="https://www.youtube.com/embed/_pZQxnkfNMA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Portraits

<Gallery :imggallery="portraits" :imgsection="module" :imgcours="slug"></Gallery>