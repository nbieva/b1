---
title: Alignements et opérations booléennes
date: 30/9/22
longtitle: Groupes et alignements
slug: alignements
disabled: false
path: cours2
thumb: tetris-thumb.jpg
caption: 
active: true
current: false
ranking: 2
lang: fr-FR
module: vectoriel
logiciel: Inkscape
extrait: Grouper et dégrouper des objets ou groupes d'objets. Options de distribution et/ou d'alignement de formes. Les opérations sur les chemins (ou opérations booléennes) dans Inkscape. Union, différence, intersection, exclusion, découpe, etc. 
content:
    - Grouper/dégrouper
    - Alignements
    - Objets et chemins
mediasgrid: 1
medias:
    - path: anni3.jpeg
      absolute: false  
      caption: Textile design by Anni Albers, in Tapis et Tissus présenté par Sonia Delaunay, Edition d'Art Charles Moreau, Paris, 1929
    - path: tetrisgif.gif
      absolute: false  
      caption: Tetris, Nintendo Game Boy - 1989
    - path: polaroid.jpg
      absolute: false  
      caption: Polaroid mark and product identity by Paul Giambarba, 1958 - 1977
      width: 50
    - path: kelly2.jpg
      absolute: false  
      caption: Ellsworth Kelly, Spectrum IX (2014)
      width: 50
    - path: https://user-images.githubusercontent.com/11083514/41197881-e862e07a-6c62-11e8-89d6-32915e52bece.png
      absolute: true
      caption: Ink/Stitch vise à être une plateforme à part entière de création de motifs de broderie basée entièrement sur du logiciel libre open source. 
    - path: https://awarewomenartists.com/wp-content/uploads/2017/12/aurelie-nemours_carre-dangle-i_1985-1991_aware_women-artists_artistes-femmes.jpg
      absolute: true  
      caption: Aurelie Nemours
    - path: kelly.jpg
      absolute: false
      caption: Elsworth Kelly
      link: 
      label:  
exercice: 
    slug: anni-albers
    label: Anni Albers
    thumb: 
    rank: 1
    horscours: true
---

## Nous savons..

- Nous y retrouver dans l'interface d'Inkscape
- Modifier les propriétés d'un document
- Créer des **formes simples** (+ coins arrondis)
- Les transformer (échelle, rotation..)
- Modifier **Remplissage et contour**
- Appliquer une **transformation simple** (modifier l'échelle ou l'orientation)
- Réordonner des objets via le panneau *Calques et objets*
- Différence entre **Formes et chemins**
- **Importer une image** bitmap dans notre document
- **Modifier l'opacité** d'un calque
- **Verrouiller** un calque
## Aujourd'hui

+ Mode d'affichage: **contours** (+ X-Ray et mode d'affichage scindé)
+ **Sélection par le toucher**
+ **Sélections multiples** (Étendre et réduire des sélections)
+ **Dupliquer** (Cmd + D) - **Cloner** (Alt + D)
+ **Pavages de clones**
+ **Alignements**
+ Activer les **poignées d'alignement** dans l'espace de travail
+ Sélection comme un groupe
+ Alignements relatifs à ..
+ **Eparpiller les centres** aléatoirement
+ **Grouper/dégrouper**
+ Modifier le centre de rotation (Maj)
+ Exporter
+ Motifs et **motifs** en raccordement
+ Modes de fusion


### Quelques raccourcis

- **Tab** pour sélectionner un objet après l'autre
- Transformer en maintenant les proportions: **Ctrl** + transformation
- **Alt + Clic** (plusieurs fois) : Sélectionner les objets les uns au-desus dessous des autres..
- **Cmd + D** : Dupliquer sur place (l'objet reste sélectionné)
- **Cmd + G** : grouper \(associer\)
- **Shift + Cmd + G** : dégrouper \(dissocier\
- et bien sûr **Cmd + S** : Sauvegarder

### Quelques liens:

- [Ink/Stitch](https://inkstitch.org/fr/)
- [FAQs Inkscape](https://inkscape.org/fr/apprendre/faq/)
- [Raccourcis clavier Inkscape](https://inkscape.org/fr/doc/keys048.fr.html)
- [Open Emojis](https://openmoji.org/)
- [https://github.com/djaiss/mapsicon](https://github.com/djaiss/mapsicon)
- [http://www.estherstocker.net/](http://www.estherstocker.net/)
- [Aurélie Nemours](https://www.google.com/search?q=aur%C3%A9lie+nemours&sxsrf=ALeKk02tkd9wujab3FfllEL0v0h1DX1TMw:1600940277000&source=lnms&tbm=isch&sa=X&ved=2ahUKEwiihpGkv4HsAhWEMewKHRHYAt4Q_AUoAXoECBwQAw&biw=2036&bih=1331#imgrc=yBjHzz9StcaEQM&imgdii=Nc618HOwYk-QwM)

### Inkscape

- [Inkscape for Adobe Illustrator users](https://wiki.inkscape.org/wiki/index.php/Inkscape_for_Adobe_Illustrator_users)
- [L'essentiel d'Inkscape](https://www.lafabriqueduloch.org/wp-content/uploads/2019/03/Tuto1_Inkscape_MS56.pdf) (Tout ce dont nous avons besoin pour commencer..)
- [Inkscape, la base, par Inkscape](https://inkscape.org/fr/doc/tutorials/basic/tutorial-basic.html)
- [Alignements et distribution d'objets dans Inkscape](http://tavmjong.free.fr/INKSCAPE/MANUAL_v14/html_fr/Align.html)

### Dans Illustrator

- Transformer en maintenant les proportions: **Shift** + transformation
- **Cmd + Alt + déplacement** : dupliquer/placer
- **Cmd + D** : dupliquer la dernière transformation (qui peut être un déplacement)
- **Cmd + G** : grouper \(associer\)
- **Shift + Cmd + G** : dégrouper \(dissocier\
- et bien sûr **Cmd + S** : Sauvegarder
