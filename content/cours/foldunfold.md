---
title: Fold/Unfold
slug: foldunfold
disabled: false
path: foldunfold
thumb: thumb.jpg
active: false
ranking: 16
lang: fr-FR
module: edition
logiciel: Adobe Indesign
extrait: Quelques références et inspirations concernant le travail de fin de quadri Fold/Unfold. On évoque ici aussi une proposition de méthode et/ou quelques étapes de travail.
content:
    - Indesign
    - gabarits
mediasgrid: 1
medias:
    - path: https://www.closky.info/images/Claude%20Closky,%20%E2%80%98Mise%20%C3%A0%20Plat%E2%80%99,%202012,%20Paris,%20Oscillations,%20No%201,%20pp.%2037-40%20(page%2038-39).jpg
      absolute: true
      caption: Claude Closky, ‘Mise à plat [straighten out]’, 2012, Paris - Oscillations, no. 1, p. 37-40.
      link: https://ww.closky.info/?p=3197
      label:
    - path: https://dongolablog.files.wordpress.com/2017/11/john-baldessari-lawrence-weiner-the-metaphor-problem-again-800x800.jpg
      absolute: true
      caption: John Baldessari & Lawrence Weiner – The Metaphor Problem- Again 1999
      link:
      label:  
    - path: ed.png
      absolute: false
      caption: Ed Ruscha (b. 1937), Leporello, Sunset Strip Buildings, 1966
      link: 
      label:
    - path: https://pbs.twimg.com/media/Ersuny1XYAIrboz.jpg
      absolute: true
      caption: Olafur Eliasson, Your house, 2006
      link: https://www.google.com/search?q=Olafur+Eliasson,+Your+house,+2006&rlz=1C5CHFA_enBE999BE999&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjFv5CS1dj7AhXShv0HHemnAY8Q_AUoAXoECAEQAw&biw=1636&bih=1531&dpr=2#imgrc=wBA3Yz0kBrLN5M&imgdii=1OL5CXNvyWqW7M
      label:
    - path: https://doc.macval.fr/Default/basicimagedownload.ashx?itemGuid=54F933FD-3CC5-481E-A7EF-6A80DAD03D33
      absolute: true  
      caption: USA 76 - de Jacques Monory (édité par le Club du livre à Paris en 1976. 300 exemplaires numérotés et signés mais seulement 150 fabriqués). Avec Michel Butor, ils créent ce « Bicentenaire kit » à l’occasion du 200e anniversaire de la déclaration d’indépendance des Etats-Unis.
      link:
      label:
    - path: http://2.bp.blogspot.com/-HfJRR8yPrCY/VZJGRGIbHAI/AAAAAAAAaV0/vGdqnIT8CAc/s1600/%2522The%2BConquest%2Bof%2BSpace%2B%2528Atlas%2Bfor%2Bthe%2Buse%2Bof%2Bartists%2Band%2Bsoldiers%2529%2522%2Bby%2BMarcel%2BBroodthaers%252C%2B1975.jpg
      absolute: true
      caption: La conquête de l’Espace/Atlas à l’usage des artistes et des militaires
      link: 
      label:
    - path: https://www.lespressesdureel.com/img_hd/img/ouvrage/9495/7.jpg
      absolute: true
      caption: Ghita Skali - Narrative Machines – Professional magazine of medical fictions
      link: https://www.lespressesdureel.com/ouvrage.php?id=9495&menu=0
      label:
---

## Aujourd'hui

+ Retour sur vos cartes postales
+ Fold/Unfold: aspects organisationnels (premières lectures, etc.)
+ Les images dans Indesign
+ [Matière première XR6](./zancan.zip)


## XR6

+ Min une planche (2 pages en vis-à-vis)
+ Pourquoi pas aussi une couverture et une quatrième de couverture
+ une grille de repère
+ Du texte
+ min 3 styles de paragraphe
+ min 2 styles de caractères
+ min 4 images
+ Min 1 habillage de texte
+ Création d'un assemblage à la fin, que vous glissez dans le drive

## Le livre-objet

"Livre ou objet ? Livre d’artiste ou sculpture ? Le terme englobe une production hétérogène. Il apparait pour la première fois en français en 1936 : l’écrivain Georges Hugnet l’utilise pour qualifier ses créations livresques. Les surréalistes poursuivent avec leur concept de boîte surréaliste, puis Fluxus dans les années 60. Mais un autre courant s’intéresse au livre « conçu comme un objet en forme de livre mais affranchi de tout souci littéraire » où le travail plastique prend le pas sur le contenu. Les artistes s’éloignent des techniques traditionnelles du livre (reliure, couverture…) et utilisent des matériaux nouveaux (plastique, métal...), s’inspirant des pratiques des Nouveaux Réalistes (accumulations) ou de l’Arte Povera (matériaux de récupération). La forme s’éloigne parfois du livre, tout en faisant référence à son histoire."
> Le livre-objet, [MACVAL](https://doc.macval.fr/default/histoire-du-livre-dartiste.aspx)


## Proposition d'étapes..

+ Trouver une **idée lumineuse**, un sujet qui vous préoccupe, en lien ou non avec votre travail d'atelier.
+ Aller voir un **imprimeur**. Réfléchir sur le **format** ET le type de **papier**. Un imprimeur pourra vous conseiller sur ces différents points. (Pensez la forme en fonction du fond)
+ Création de vos **textes**
+ Recherche de **polices** de caractères
+ Chemin de fer (papier/crayon) + maquettes
+ Création et travail de vos **images** (bitmap et vectorielles). Attention à la question de la qualité des images.
+ Création de vos **grilles** de repères, gabarits et **styles de paragraphes et caractères**.
+ Positionnement de vos **blocs**
+ Placement et travail de vos contenus
+ **Tests** réguliers d'impression à 100% (même en brouillon noir et blanc)
+ **Assemblage**
+ Tests d'impression final
+ Impression
+ Façonnage

## Liens intéressants

+ [https://revue-faire.eu/fr/](https://revue-faire.eu/fr/)

### Les différents types d'éditions/reliure

Vous trouverez pas mal d'infos sur les **différents types d'éditions** [sur le site de Baptiste](http://cours.baptiste-tosi.eu/doku.php?id=cours_12) ainsi que, sur cette même page également, différents **lieux pour imprimer** votre édition à Bruxelles.