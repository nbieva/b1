---
title: La plume
date: 7/10/22
longtitle: Courbes de Bézier et courbes Spiro
slug: plume
path: cours3
thumb: thumb.jpg
active: false
ranking: 3
lang: fr-FR
module: vectoriel
logiciel: Inkscape
extrait: Découverte des courbes de Bézier, courbes Spirographiques. Nous découvrirons aussi les différentes façons de créer, manipuler ou corriger nos tracés à l'aide de l'outil d'édition des noeuds.
content:
    - Dessin à la plume
    - Manipuler les noeuds
    - Noeuds doux et durs
mediasgrid: 1
medias:
    - path: diy.jpg
      absolute: false
      caption: Do It Yourself (Violin, Sailboat, Seascape, Flowers), 1962 © Andy Warhol  (1928 - 1987)
    - path: a-nemours.jpg
      absolute: false  
      caption: Aurelie Nemours - No 16 ou XVI, diptyque, huile sur toile, 1968
    - path: kelly1.jpeg
      absolute: false  
      caption: Elsworth Kelly, Flowers
    - path: Alex-Katz_Brisk-Day.jpg
      absolute: false  
      caption: Alex Katz, Brisk Day, 1990 (Xylogravure / Aquatinte / Lithographie)
      link: https://www.google.be/search?q=alex+katz&rlz=1C5CHFA_enBE752BE753&source=lnms&tbm=isch&sa=X&ved=0ahUKEwitor7yy_zYAhWMyKQKHd-vAbsQ_AUICigB&biw=1307&bih=709#imgrc=_
      label: Lien
gallery:
    - path: arntz-01.jpeg
      absolute: false  
      caption:
    - path: arntz-02.jpg
      absolute: false  
      caption:
    - path: arntz-03.jpg
      absolute: false  
      caption:
exercice: 
    slug: portrait
    label: Portrait
    thumb: 
    rank: 1
    horscours: true
---

## Les courbes de Bézier par l'exemple..

+ [Animated Bézier Curves - Jason Davies](https://www.jasondavies.com/animated-bezier/)
+ [http://math.hws.edu/eck/cs424/notes2013/canvas/bezier.html](http://math.hws.edu/eck/cs424/notes2013/canvas/bezier.html)
+ [https://www.desmos.com/calculator/cahqdxeshd](https://www.desmos.com/calculator/cahqdxeshd)
+ [https://bezier.method.ac/](https://bezier.method.ac/)

## Au programme

+ Récapitulatif des 2 premiers cours + liens ci-dessous
+ Retours sur les exercices + fonds perdus
+ Importation d'image et modèles
+ Affichage (Default, Large, Personnalisé)
+ Changer le background du workspace (dark) -> Propriétés du document en bas à gauche
+ Objet > organiser
+ Miroirs (v & h)
+ Alt+Clic pour sélectionner par le toucher.
+ Déplacer les dégradés ou motifs avec les objets
+ Palettes de couleur personalisées
+ La **plume** et **opérations sur les chemins**: deux moyens d'enrichir notre vocabulaire formel
+ Qu’est-ce que la plume?
+ Les courbes de Bézier
+ Manipulation de points et outil noeuds
+ Tracés ouverts et fermés + Jonctions de tracés
+ Tracés contraints
+ Dessin à la plume: principaux raccourcis clavier
+ Transformations : Miroirs

## Quelques liens supplémentaires

+ [Inkscape pour la fabrication numérique](https://fablabo.net/wiki/Inkscape_pour_la_fabrication_num%C3%A9rique)
+ [InkStitch](https://inkscape.org/fr/~wwderw/%E2%98%85inkstitch-embroidery-extension)
+ [Raccourcis Clavier](https://inkscape.org/fr/doc/keys.html)
+ [Inkscape extensions](https://inkscape.org/fr/gallery/=extension/)
+ [makercase.com](https://fr.makercase.com/#/)
+ [Ajouter des thèmes](https://www.youtube.com/watch?v=mGLLVDFFR5o)
+ [Générateur de Palettes](https://github.com/olibia/inkscape-generate-palette)
+ [Open Emojis](https://openmoji.org/)


## Quelques exercices pour vous faire la main au cours.

### 1. Fleur de lys

+ Réalisez, à l'aide des outils vus au cours, **les drapeaux repris sur [cette page](/exercices/quebec)** .
+ Transferez vos fichiers vectoriels dans votre dossier sur le Drive (nommez vos fichiers comme suit **nom-atelier-quebec.svg**, par exemple)
+ Gardez un oeil sur vos calques/objets pour garder votre fichier le plus "propre" possible.
+ Songez aux alignements et aux groupes ainsi qu'aux transformations en miroir.

### 2. Gerd Arntz

+ Réalisez, à l'aide des outils vus au cours, au moins 3 animaux ou personnages de Gerd Arntz.
+ Transferez vos fichiers vectoriels dans votre dossier sur le Drive (nommez vos fichiers comme suit **nom-atelier-gerd.svg**, par exemple)
+ Gardez un oeil sur vos calques/objets pour garder votre fichier le plus "propre" possible.

<Gallery :imggallery="gallery" :imgsection="module" :imgcours="slug"></Gallery>

### 3. Paul Rand

+ Recréez ce visuel de Paul Rand à l'aide des outils vus au cours. Voir [cette page](/exercices/paul-rand)
### 4. OpenMojis

+ Créez 3 emojis qui pourraient venir s'ajouter à la bibliothèque libre [OpenMojis](https://openmoji.org/), en suivant leurs quelques règles reprises ici: [https://openmoji.org/styleguide/](https://openmoji.org/styleguide/)
+ La taille de votre document est libre
+ Il doit s'agir bien évidemment d'emojis utilisant principalement la plume




