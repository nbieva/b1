---
title: Ressources Cartographie
date: 
slug: cartographie
disabled: false
thumb: carto-thumb.jpg
ranking: 5
module: vectoriel
artistique: true
logiciel: Inkscape
extrait: Une cartographie imaginaire, subjective, personnelle, politique, sociale, émotionnelle, écologique… Cette page reprend quelques ressources supplémentaires. Le briefing complet est sur Digitalab.be
mediasgrid: 2
medias:
    - path: https://interlude-cdn-blob-prod.azureedge.net/interlude-blob-storage-prod/2014/02/ligeti-artikulation.jpg
      absolute: true
      caption: György Ligeti, Artikulation (Partition de Rainer Wehinger)
      link: 
      label: 
    - path: dante.png
      absolute: false  
      caption: Mélanie G. / Dessin
    - path: nathaliemiebach.jpg
      absolute: false
      caption: Since 2009, I’ve been translating weather data into 2D and 3D musical scores that become blueprints for my sculptures. They are also used in collaborations with composers through the Weather Score Project. Nathalie Miebach (https://www.nathaliemiebach.com/)
      link: 
      label: 
    - path: charlotte-q.jpg
      absolute: false
      caption: Charlotte Q. / Dessin
      link: 
      label: 
    - path: viegas.jpg
      absolute: false
      caption: Fernanda Viegas & Martin Wattenberg, Poster for Wired Magazine, 2008
      link: 
      label: 
      width: 50
gallery:
    - path: openstreetmap.jpg
      absolute:
      caption: L'interface d'édition d'OpenStreetMap
    - path: inkscapeinterface.jpg
      absolute:
      caption: L'interface d'Inkscape 1.2
exercice: 
    slug: mullican
    label: Matt Mullican
    thumb: 
    rank: 1
    horscours: true
---

Vous trouverez ci-dessous quelques documents, que nous évoquerons au cours, en rapport avec votre travail de Cartographie Imaginaire. Ce travail est à voir comme un travail de visualisation de données. Il concerne donc tout type de données pouvant être mises en formes. Il peut donc concerner des données géographiques, météorologiques ou GPS, certes, mais aussi la musique, la danse, les sciences, le sport, la littérature, l'art, l'histoire, le voyage, le théâtre, l'architecture, etc. 

Songez à une thématique ou un sujet qui vous intéresse ou vous passionne. Quelles données peuvent en être extraites? Quelles mise en forme visuelle? Pour dire quoi? Dans quel contexte?

Pour rappel, vous trouverez le briefing de ce travail avec ses quelques contraintes techniques sur [Digitalab.be](http://www.digitalab.be/briefing-cartographie-imaginaire-2018-2019/)

<figure>
  <img src="/cours/vectoriel/cartographie/paley.jpg" alt="Le code d'un fichier SVG">
  <figcaption>
    <span>Relationships among Scientific Paradigms, Bradford Paley</span>
      <div class="img-actions">
        <a href="/cours/vectoriel/cartographie/paley.jpg" download><svg xmlns="http://www.w3.org/2000/svg" height="20" width="20" fill="#ff1e00"><path d="M5.667 16.229q-.792 0-1.334-.552-.541-.552-.541-1.323v-1.292h1.625v1.292q0 .084.083.167t.167.083h8.666q.084 0 .167-.083t.083-.167v-1.292h1.625v1.292q0 .771-.552 1.323-.552.552-1.323.552Zm4.354-3.396L5.792 8.604l1.166-1.125 2.25 2.25V2.5h1.625v7.229l2.25-2.25 1.167 1.125Z"/></svg></a>
        <a href="/cours/vectoriel/formes/inkscape12-2.jpg" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" height="20" width="20" fill="#ff1e00"><path d="M4.625 17.25q-.771 0-1.323-.552-.552-.552-.552-1.323V4.625q0-.771.552-1.323.552-.552 1.323-.552h5.458v1.625H4.625q-.083 0-.167.083-.083.084-.083.167v10.75q0 .083.083.167.084.083.167.083h10.75q.083 0 .167-.083.083-.084.083-.167V9.917h1.625v5.458q0 .771-.552 1.323-.552.552-1.323.552Zm3.396-4.125-1.146-1.146 7.604-7.604h-2.083V2.75h4.854v4.854h-1.625V5.521Z"/></svg></a>
      </div>
  </figcaption>
</figure>


"Une présentation de la façon dont différents paradigmes de la science interagissent. Le travail était une collaboration entre Kevin Boyack, John Burgoon, Peter Kennard, Dick Klavans et moi.

L'image a été construite en triant environ 800 000 articles scientifiques en 776 paradigmes scientifiques différents (nœuds circulaires colorés) en fonction de la fréquence à laquelle les articles ont été cités ensemble par les auteurs d'autres articles. Des liens (lignes courbes) ont été établis entre les paradigmes qui partageaient des membres communs, puis mis en place pour fonctionner comme des élastiques, rapprochant les paradigmes similaires les uns des autres lorsqu'une simulation physique a fait que chaque paradigme se repousse : ainsi la disposition découle directement des données. Les grands paradigmes ont plus de papiers. Les étiquettes répertorient les mots communs propres à chaque paradigme.

Publié dans Nature, Discover Magazine, SEED, Geo (édition Europe de l'Est et Inde, Brésil, etc.), et de nombreux livres et articles."

**Bradford Paley, sur [http://wbradfordpaley.com/](http://wbradfordpaley.com/)**