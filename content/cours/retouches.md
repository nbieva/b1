---
title: Outils de retouches
slug: retouches
disabled: false
date: 25/11/22
path: retouches
thumb: thumb.jpg
active: false
ranking: 11
lang: fr-FR
module: bitmap
logiciel: Krita
extrait: Les outils de retouches dans Krita. Tampon de duplication et correcteurs. Nous verrons comment, à l'aide de ces outils, supprimer certains éléments d'une image et dupliquer des textures..
content:
    - Krita
    - retouches
mediasgrid: 1
medias:
    - path: annelaureetienne.png
      absolute: false
      caption: Anne-Laure Etienne
    - path: torrechiara.jpg
      absolute: false
      caption: Utilisez cette image comme base..
    - path: castle-retouche.jpg
      absolute: false
      caption: Un résultat possible
    - path: sculpturedujardin.jpg
      absolute: false   
      caption: Filip Dujardin
gallery:
    - path: ale.png
      absolute:
      caption: Anne-Laure Etienne
    - path: ale2.png
      absolute:
      caption: Anne-Laure Etienne
filip:
    - path: Filip-Dujardin-Sequence-1-0.jpg
      absolute:
      caption: Filip Dujardin
    - path: Filip-Dujardin-Sequence-1-1.jpg
      absolute:
      caption: Filip Dujardin

---





## Les liens du jour

+ [Galerie Cartes postales 2021](https://galerieb1.netlify.app/)
+ [Anne-Laure Etienne](https://www.annelaure-etienne.com/tissueandbones)
+ [Filip Dujardin](https://www.google.com/search?q=filip+Dujardin&rlz=1C5CHFA_enBE999BE999&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjh4Iebr8f7AhUFHewKHcMCDxwQ_AUoAXoECAIQAw&biw=1956&bih=1588&dpr=2)
+ [Midjourney](https://www.midjourney.com/)
+ [Dall-e (open-ai)](https://openai.com/dall-e-2/)
+ [Stable Diffusion](https://huggingface.co/spaces/stabilityai/stable-diffusion)
+ [Prompter Guide pour Midjourney](https://prompterguide.com/)

## Nous savons:

- Nous y retrouver dans l'**interface** de Krita
- Ce que sont **résolution, définition et taille d'image**.
- Le fonctionnement des **calques** dans Krita (pareil ailleurs)
- Renommer un calque / grouper des calques
- Importer/copier une image dans un document existant.
- Opérer des **transformations** de base
- Utiliser les **outils de sélection** de base (Sélection rectangulaire, lasso, etc..)
- Créer une **sélection par la couleur**
- Ajouter et utiliser un **masque de transparence (ou de fusion dans PSD)**
- Ajouter et utiliser un **masque de filtrage (ou calque de réglage dans PSD)**
- Convertir une image en masque de transparence ou de filtrage
- Ajouter et utiliser un **masque de remplissage (ou calque de remplissage dans PSD)**
- Manipuler un minimum nos **brosses** ou pinceaux
- Les **modes de fusion**

## Aujourd'hui

- Rappel **Carte postale**
- Rappel [Licenses Indesign](https://docs.google.com/forms/d/e/1FAIpQLSe9eTCnrifKvgu6SRvJhBoV58MpWNpGzE1QpPIzGXB--cyiYA/viewform?vc=0&c=0&w=1&flr=0) (vous devez être connecté.e avec votre adresse de l'école)
- Outils de retouche: le **clonage** dans Krita
- Gomme intelligente (outil **correcteur**)
- Les **styles de calques**
- Midjourney etc. Crazy Brutalist architecture
- En **pratique**: Quelques exercices
- Beni Bischof, Filip Dujardin, Nicolas Moulin

## Quelques images avec lesquelles travailler

Réalisez un ou deux des exercices ci-dessous. Déposez vos fichiers .kra dans notre dossier partagé à la fin du cours.
Vous devrez la plupart du temps faire usage de la brosse de clonage, de la gomme intelligente (Correcteur), des masques de transparence et des masques de filtrage.

+ Le [Château](https://b1.netlify.app/bitmap/retouches.html#la-chateau-de-torrechiara)
+ [Retouches](https://b1.netlify.app/bitmap/retouches.html#retouches-et-correcteur)

### Anne-Laure Etienne

Supprimez le sujet au centre de l'image ci-dessous (et remplacez-le éventuellement par un autre au choix dans [cette galerie photo](https://www.annelaure-etienne.com/tissueandbones). A vous de sélectionner une image adéquate.

<Gallery :imggallery="gallery" :imgsection="module" :imgcours="slug"></Gallery>
### La Château de Torrechiara

A partir de l'image HD ci-contre (2), modifiez l'architecture du Château en supprimant/rajoutant des fenêtres, tours ou autres éléments afin de recréer une toute autre configuration. Le résultat n'est pas forcément un bâtiment aveugle comme l'image 3. Ce peut être tout le contraire..

### Filip Dujardin

Reconstituez une seule image à partir de ces deux images du travail de Filip Dujardin. Les calques de réglages(ou masques de filtrages dans Krita) seront importants ici pour ajuster les deux images.

<Gallery :imggallery="filip" :imgsection="module" :imgcours="slug"></Gallery>

### Anthony Gormley

Supprimez au moins un des deux personnages de cette image.

![](/cours/bitmap/retouches/gormley01.jpg)


### Retouches et correcteur

Nettoyez l'une des images ci-dessous.

<a data-fancybox title="Retouche" href="/assets/Portrait_of_Mrs_Elizabeth_Thomas_(4671178).jpg">![Retouche](/cours/bitmap/retouches/Portrait_of_Mrs_Elizabeth_Thomas_(4671178).jpg)</a>

<a data-fancybox title="Retouches" href="/assets/retouches02.jpg">![Retouches](/cours/bitmap/retouches/retouches02.jpg)</a>

![Retouches](/cours/bitmap/retouches/retouches03.jpg)

