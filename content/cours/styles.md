---
title: Styles et imports d'images
slug: styles
disabled: false
path: styles
thumb: thumb.jpg
active: false
ranking: 14
lang: fr-FR
module: edition
logiciel: Adobe Indesign
extrait: On va un peu plus loin avec les blocs de textes et on découvre la création et l'édition de styles de paragraphes et de caractères. Des outils que l'on retrouvera dans d'autres logiciels...
content:
    - Indesign
    - gabarits
mediasgrid: 1
medias:
    - path: http://artshebdomedias.com/wp-content/uploads/carl_andre_yucatan1.jpg
      absolute: true
      caption: Carl Andre, Yucatan (détail), Carl Andre, 1972
      link: https://www.artshebdomedias.com/article/280115-carl-andre-new-york-mots-sculptes/
      label: 
    - path: https://www.pixartprinting.it/blog/wp-content/uploads/2019/11/Dieter-Roth_Little-temptative-recipe.jpg
      absolute: true
      caption: “Little Tentative Recipe” est un livre miniature, de forme cubique, contenu dans une boîte créée à l’origine pour y ranger les sachets de thé. À l’intérieur, on trouve 800 impressions couleur offset réalisées par les étudiants de la Watford School of Art en suivant les indications (la recette !) de Dieter Roth. (src https://www.pixartprinting.fr/)
      link: 
      label:
    - path: https://sfmoma-media-dev.s3.us-west-1.amazonaws.com/www-media/2019/11/27141954/0376-Untitled-Fold_1000px-768x1006.jpg
      absolute: true
      caption: Tauba Auerbach - Fold series
      link: 
      label:
    - path: https://www.pixartprinting.it/blog/wp-content/uploads/2019/11/Bruno-Munari_Quadrante-Illeggibile.jpg
      absolute: true
      caption: Quadrante Illeggibile Bianco e Rosso (livre illisible blanc et rouge) composé de 40 pages en carton blanc et rouge, de différentes formes. À chaque page, le spectateur découvre une nouvelle combinaison. La sur-couverture est constituée d’une page pliée à plusieurs reprises de manière à former un système de triangles sur lequel se trouve une note biographique sur Bruno Munari traduite en 8 langues. (src https://www.pixartprinting.fr/)
      link: https://www.pixartprinting.fr/blog/livres-dartistes/
      label: 
    - path: 
      absolute: true
      caption: 
      link: 
      label: 
gallery:
    - path: book-1.jpg
      absolute: false
      caption: Electronic Superhighway, Whitechapel Gallery
    - path: book-2.jpg
      absolute: false
      caption: Electronic Superhighway, Whitechapel Gallery
---

## Textes

- **Questions éventuelles** sur le travail de fin de quadri
- Retour sur les **pages et gabarits**
- Retour sur les **Blocs de texte** et colonnes + **tracés** à la plume
- Texte de substitution + [Liste de générateurs](https://loremipsum.io/fr/ultimate-list-of-lorem-ipsum-generators/)
- Le texte en excès
- **Chaînages** de texte
- **Styles** de paragraphes et de caractères
- **Alignements** (sur la sélection, la page, les marges..)
- Trouver et installer des **polices** (Attention à les intégrer au document par la suite) - [Google Fonts](https://fonts.google.com/) et autres

<Gallery :imggallery="gallery" :imgsection="module" :imgcours="slug"></Gallery>
## Images

+ Un point sur la qualité des **images**
+ Insertion d'**images**
+ [Grilles de mise en page](https://www.nundesign.fr/fondamentaux-graphiques/grille-de-mise-en-page-principe-et-utilisation)
+ Rappel: gestion des blocs d'images + tracés à la plume : valables aussi pour les images
+ **Imports d'images** + **imports multiples** (dans les blocs ou à la volée)
+ **Ajustements**
+ **Performances d'affichage**
+ **Habillages** de texte + Les utiliser avec du texte exclusivement
+ Le [point typographique](https://fr.wikipedia.org/wiki/Point_(unit%C3%A9)#Point_DTP_ou_point_pica)
+ **Numérotation** automatique
+ **Exports** PDF
+ **Assemblages**
+ Questionnaire Fold

## Liens intéressants

+ Les [formats](https://papersizes.io/)
+ Les [nombreuses et splendides couvertures](http://cover.baptiste-tosi.eu/) que Baptiste a mis en ligne pour vous.
+ Sur les [grilles de mise en page](https://www.nundesign.fr/fondamentaux-graphiques/grille-de-mise-en-page-principe-et-utilisation)
+ Les [poemes de Carl Andre](https://www.google.com/search?q=carl+andre+poemes&sxsrf=ALeKk01DugxnP-7_8qW3R57oOlww_3vSdg:1607024642107&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjTgcCnybLtAhVRKuwKHTg-BHgQ_AUoAXoECAYQAw&biw=1199&bih=1338)
+ Quelques exemples de [grille modulaire](http://yharel.free.fr/data/informatique/logiciels/bureau/traitement_de_texte/typographie/mise_page_modulaire.htm)
+ [Grille de Mise en page (British Rail)](https://www.designspiration.com/save/505229180583/)
+ Quelques [travaux](https://galerieb1.netlify.app/indd)
+ [Design layouts](https://www.designspiration.com/ovsyannykov/design-layouts/?marker=7744992847296)
+ [25 Hours Hotel Magazine](https://www.behance.net/gallery/20305115/Companion-No2-25Hours-Hotel-Magazine)
## Où trouver des typographies libres de qualité?

+ [Font Library](https://fontlibrary.org/), un agrégateur de fontes sous licences libres
+ [VTF](http://velvetyne.fr/), une fonderie libre
+ [use&modify](http://usemodify.com/), une sélection de fontes libres par l'artiste [Raphaël Bastide](https://raphaelbastide.com/)
+ [Font Squirrel](https://www.fontsquirrel.com/), un autre agrégateur de fontes libres

----------

### Quelques conseils

+ Réunir et préparer son matériel visuel et textuel.
+ Faites un croquis (votre chemin de fer). Construisez votre mise en page à la main, en réunissant vos différentes idées.
+ Trouver une ou deux polices qui correspondent à l'univers que vous voulez créer.
+ Ayez à l'esprit l'ensemble des outils vus au cours (plume, masques d'écrétage, modes de fusion)
+ Ne surchargez pas (sauf si cela sert votre propos). Pensez aux vides.
+ Construisez le tout sur une grille qui vous permette d'être créatifs