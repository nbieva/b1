---
title: Masques de filtrage / réglages
slug: filtrages
disabled: false
path: filtrages
date: 18/11/22
thumb: color.jpg
active: false
ranking: 10
lang: fr-FR
module: bitmap
logiciel: Krita
extrait: Découverte des masques et calques de filtrages dans Krita. Correction de la lumière et des couleurs, entre autres. Nous aborderons éventuellement aussi les masques de colorisation.
content:
    - Krita
    - filtrages
mediasgrid: 1
medias:
    - path: reglages02.jpg
      absolute: false  
      caption: Olafur Eliasson / Room for one colour, 1997 / Moderna Museet, Stockholm 2015 / Photo Ander Sune Berg
      link: 
      label:
    - path: https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/Prokudin-Gorskii-12.jpg/843px-Prokudin-Gorskii-12.jpg
      absolute: true  
      caption: Portrait of Sergei Mikhailovich Prokudin-Gorskii. Early color photograph from Russia, created by Sergei Mikhailovich Prokudin-Gorskii as part of his work to document the Russian Empire from 1904 to 1916.
      link: https://commons.wikimedia.org/wiki/File:Prokudin-Gorskii-12.jpg
      label:
    - path: https://upload.wikimedia.org/wikipedia/commons/f/f3/Alleia_Hamerops_composite.jpg
      absolute: true  
      caption: Extrait de Alleia Hamerops - les trois images élémentaires et la résultante couleur
      link: 
      label:

exercice: 
    slug: filtrages
    label: filtrages
    thumb: 
    rank: 1
    horscours: true
---

Les liens du jour:

+ [James Turrell / Skyspace](https://www.thecollector.com/james-turrell-skyspace-art/)

-----

+ Retour sur vos **exercices**
+ Questions sur la **carte postale** ([Une page avec de belles références](http://caso.baptiste-tosi.eu/?cours=6) sur le site de Baptiste. [Une autre avec quelques travaux](https://galerieb1.netlify.app))
+ Retour sur les masques de transparence
+ Conversion en masque de transparence (calque du dessus dans la pile)
+ Calques/masques de **filtrage** et calques de **remplissage**
+ Appliquer un calque de filtrage à certains calques seulement (groupes)
+ [Sergueï Prokoudine-Gorski](https://fr.wikipedia.org/wiki/Sergue%C3%AF_Prokoudine-Gorski) + [https://commons.wikimedia.org/wiki/File:Prokudin-Gorskii-12.jpg](https://commons.wikimedia.org/wiki/File:Prokudin-Gorskii-12.jpg)
+ Niveau, Histogramme et Canaux RVBA (sur principe des masques)
+ Teinte/Saturation
+ Calques de remplissage
+ Modes de mélanges
+ "Peindre" un réglage. (Visages)
+ Masques de transformation

La video ci-dessous reprend le principe de base des calques et masques de filtrages

<iframe width="740" height="460" src="https://www.youtube.com/embed/-44Is87mM0Y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Une autre sur les modes de mélange et les styles de calques

<iframe width="740" height="460" src="https://www.youtube.com/embed/T0WQ4yd83kw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

