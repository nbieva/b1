---
title: Formes simples
date: 23/9/22
slug: formes
disabled: false
thumb: tanaka-thumb.jpg
active: true
current: true
ranking: 1
module: vectoriel
logiciel: Inkscape
extrait: Introduction au cours de B1 - Agenda et aspects organisationnels. Différences entre images matricielles et vectorielles. Le format SVG. Les logiciels libres. Introduction à Inkscape et au desin de formes simples. Grilles et guides.
mediasgrid: 2
medias:
    - path: FontanaModernMastersSet4.jpeg
      absolute: false
      caption: The Fontana Modern Masters were a series of pocket guides on the writers, philosophers, and other thinkers and theorists whose ideas were shaping the intellectual landscape of the twentieth century… (Wikipedia)
      link: http://blog.iso50.com/24603/fontana-modern-masters/
      label: Fontana Modern Masters
    - path: flags.gif
      absolute: false  
      caption: Stokholm Design Lab / Fare mondi, 53e Biennale de Venise, 2009
    - path: halley.jpg
      absolute: false
      caption: Peter Halley, a perfect plan, 2020
      link: 
      label: 
    - path: printprocess.png
      absolute: false
      caption: printprocess
      link: 
      label: 
      width: 50
    - path: laser.jpg
      absolute: false
      caption: Découpe laser à l'aide d'un fichier vectoriel
      link: 
      label: 
      width: 50
    - path: inkscape12-2.jpg
      absolute: false
      caption: Le code d'un fichier SVG
      link: 
      label: 
      width: 50
    - path: Tautonym-B-1944.jpg
      absolute: false  
      caption: Josef Albers
      link: https://www.google.com/search?q=josef+albers+structural+constellation&source=lnms&tbm=isch&sa=X&ved=2ahUKEwj1iMqfkdHuAhWj8uAKHYhfBr4Q_AUoAXoECBIQAw&biw=2086&bih=1338
      label: Structural Constellations, Josef Albers 
      width: 50 
    - path: https://d5wt70d4gnm1t.cloudfront.net/media/a-s/artworks/matt-mullican/76215-865495731629/matt-mullican-some-details-about-the-five-worlds-960x640.jpg
      absolute: true
      caption: Matt Mullican - Some details about the five worlds
      link: https://clubparadis.prezly.com/macs-presente-la-premiere-exposition-monographique-de-matt-mullican-dans-un-musee-en-belgique
      label: Matt Millican au Mac's du Grand-Hornu

gallery:
    - path: openstreetmap.jpg
      absolute:
      caption: L'interface d'édition d'OpenStreetMap
    - path: inkscapeinterface.jpg
      absolute:
      caption: L'interface d'Inkscape 1.2
exercice: 
    slug: mullican
    label: Matt Mullican
    thumb: 
    rank: 1
    horscours: true
---

## Au programme

* Présences, agenda et fonctionnement du Drive pour les remises (avec vos adresses email de l'école)
* [Vectoriel/Bitmap](https://fr.wikipedia.org/wiki/Scalable_Vector_Graphics#/media/Fichier:Bitmap_VS_SVG_Fr.svg)
* Le [format SVG](https://fr.wikipedia.org/wiki/Scalable_Vector_Graphics)
* [Icônes](https://erikflowers.github.io/weather-icons/), [Emojis](https://openmoji.org/), [SVG Animations](https://lottiefiles.com/animation/svg), ..
* Inkscape et autres [alternatives à Illustrator](https://affinity.serif.com/fr/) et [logiciels libres](http://img.scoop.it/jVXKRE8Y_NQAX_RoOOa1Ejl72eJkfbmt4t8yenImKBXEejxNn4ZJNZ2ss5Ku7Cxt) \([Inkskape](https://inkscape.org/fr/), The [Gimp](https://www.gimp.org/fr/), [Scribus](https://www.scribus.net/), [OpenStreet Map](https://www.openstreetmap.org/#map=18/50.81793/4.37322), ..\) + [FLOSS manuals](https://www.flossmanualsfr.net/)
* [Installation](https://inkscape.org/release/inkscape-1.2.1/) de Inkscape
* Inkscape: Interface, formes simples et couleurs
* Didacticiels

<Gallery :imggallery="gallery" :imgsection="module" :imgcours="slug"></Gallery>

## Artistes
 
+ [Léon Wuidar (Mu in the City)](https://www.mu-inthecity.com/dans-latelier-de-leon-wuidar)
+ [Heidi Wood, Road Signs](https://www.heidiwood.net/en/work-categories/road-signs/)
+ Bridget Riley, Elsworth Kelly, Frank Stella (Odelsk), Robert Mangold, Bauhaus, [Heidi Wood](https://www.heidiwood.net/en/all-works/), Jerôme Considérant, [Sol LeWitt](https://www.google.com/search?q=sol+lewitt&sxsrf=ALeKk01P7bORuqOp-bMcb4tdl7nxe04JHA:1611243801131&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjEnqbzrq3uAhWJCOwKHbJnAqcQ_AUoAXoECA8QAw&biw=1126&bih=1280)
+ [Peter Halley](https://www.google.com/search?q=peter+halley&tbm=isch&ved=2ahUKEwig_bqLsq3uAhXDtqQKHTHzDDcQ2-cCegQIABAA&oq=peter+halley&gs_lcp=CgNpbWcQAzICCAAyAggAMgQIABAeMgQIABAeMgQIABAeMgQIABAeMgQIABAeMgQIABAeMgQIABAeMgQIABAeOgQIIxAnOgUIABCxAzoICAAQsQMQgwE6CggAELEDEIMBEEM6BAgAEEM6BAgAEAM6BwgAELEDEENQ0M8HWMvfB2Cz4wdoAHAAeACAAVaIAewFkgECMTKYAQCgAQGqAQtnd3Mtd2l6LWltZ8ABAQ&sclient=img&ei=caQJYOCABsPtkgWx5rO4Aw&bih=1280&biw=1126)
+ [Albers, Structural constellations](https://www.google.com/search?q=josef+albers+structural+constellation&source=lnms&tbm=isch&sa=X&ved=2ahUKEwj1iMqfkdHuAhWj8uAKHYhfBr4Q_AUoAXoECBIQAw&biw=2086&bih=1338)
+ [Sol LeWitt, Etoile](https://www.google.com/search?q=sol+lewitt&sxsrf=ALeKk01P7bORuqOp-bMcb4tdl7nxe04JHA:1611243801131&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjEnqbzrq3uAhWJCOwKHbJnAqcQ_AUoAXoECA8QAw&biw=1126&bih=1280#imgrc=cnY8QrLEsIZNdM&imgdii=PEAzId17M4PgmM)
+ [Un autre..](https://www.google.com/search?q=sol+lewitt&sxsrf=ALeKk01P7bORuqOp-bMcb4tdl7nxe04JHA:1611243801131&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjEnqbzrq3uAhWJCOwKHbJnAqcQ_AUoAXoECA8QAw&biw=1126&bih=1280#imgrc=tHieEEHnEvp4dM&imgdii=v-Uzc090_3w9nM)
+ [Matt Mullican](https://www.mac-s.be/fr/expositions/matt-mullican)
+ [Johan Grimonprez, Inflight](http://www.johangrimonprez.be/main/Books_Inflight.html)
+ [Malika Favre](https://www.google.com/search?q=malika+favre&sxsrf=ALeKk01dxvzr5lSth9zKMnzcaeVSE3Lu8w:1612459852710&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjRy_CF4dDuAhUKHxoKHftcDwcQ_AUoAXoECAsQAw&biw=2252&bih=1283), [Ellsworth Kelly (Flowers)](https://www.google.com/search?sxsrf=ALeKk02_GDkWR2t0veWgE1FD1RrVJoT_Rg:1612460079483&source=univ&tbm=isch&q=ellsworth+kelly+flowers&sa=X&ved=2ahUKEwii2YHy4dDuAhWCyoUKHQbsD0oQjJkEegQIBRAB&biw=2252&bih=1283)
+ [DesignReviewed](https://designreviewed.com/archive/)
+ [Iso 50](http://blog.iso50.com/24603/fontana-modern-masters/?fbclid=IwAR2koQG4bqqQa1HBSXbXYL7oY770D3XWNB3il7aHU_JXaRq72XwQLlySsAw)
+ Anni & Josef Albers

<!-- <figure>
  <img src="/cours/vectoriel/formes/libres.png" alt="Le code d'un fichier SVG">
  <figcaption>
    <span>Le code d'un fichier SVG</span>
      <div class="img-actions">
        <a href="/cours/vectoriel/formes/inkscape12-2.jpg" download><svg xmlns="http://www.w3.org/2000/svg" height="20" width="20" fill="#ff1e00"><path d="M5.667 16.229q-.792 0-1.334-.552-.541-.552-.541-1.323v-1.292h1.625v1.292q0 .084.083.167t.167.083h8.666q.084 0 .167-.083t.083-.167v-1.292h1.625v1.292q0 .771-.552 1.323-.552.552-1.323.552Zm4.354-3.396L5.792 8.604l1.166-1.125 2.25 2.25V2.5h1.625v7.229l2.25-2.25 1.167 1.125Z"/></svg></a>
        <a href="/cours/vectoriel/formes/inkscape12-2.jpg" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" height="20" width="20" fill="#ff1e00"><path d="M4.625 17.25q-.771 0-1.323-.552-.552-.552-.552-1.323V4.625q0-.771.552-1.323.552-.552 1.323-.552h5.458v1.625H4.625q-.083 0-.167.083-.083.084-.083.167v10.75q0 .083.083.167.084.083.167.083h10.75q.083 0 .167-.083.083-.084.083-.167V9.917h1.625v5.458q0 .771-.552 1.323-.552.552-1.323.552Zm3.396-4.125-1.146-1.146 7.604-7.604h-2.083V2.75h4.854v4.854h-1.625V5.521Z"/></svg></a>
      </div>
  </figcaption>
</figure> -->

## Points techniques

* Nouveau document
* Environnement de travail et interface d'Inkscape (Préférences)
* Propriétés du document (Maj + Cmd/Ctrl + D)
* Modifier la couleur du background (canvas)
* Métadonnées + License de vos fichiers
* Affichage > Large
* Formes simples
* Selection (et avec Alt)
* Outils de sélection S (selection) et N (nodes)
* Boutons de sélection
* Guides, grilles, magnétisme
* Modes d'affichage (Normal, contour, alterner (Cmd+5))
* Transformation d'objets
* Objets et chemins
* Contour et remplissage + dégradés de couleurs
* Fenêtre des calques et objets
* Disposition avant/arrière. Ordre d'empilement.
* Verrouiller/Dupliquer/masquer
* Utilisation d'une image comme modèle (verrouiller, modifier l'opacité, passer à l'arrière-plan)

Dans un premier temps,  enregistrez vos fichiers au format .svg (Inkscape). Nous verrons ensuite comment les exporter si nécessaire.

### Un objet vectoriel peut:

+ se transformer
+ changer de couleur de fond/de contour (n'oubliez pas l'opacité)
+ se dupliquer
+ s'aligner
+ Se déplacer dans la pile des objets/calques
+ se verrouiller/déverrouiller
+ (s'animer)

## Ressources

+ [L'essentiel d'Inkscape](https://www.lafabriqueduloch.org/wp-content/uploads/2019/03/Tuto1_Inkscape_MS56.pdf) (Tout ce dont nous avons besoin pour commencer..)
+ [Inkscape for Adobe Illustrator users](https://wiki.inkscape.org/wiki/index.php/Inkscape_for_Adobe_Illustrator_users)
+ [Apprendre Inkskape](https://inkscape.org/fr/apprendre/)
+ [Transformations](https://daviesmediadesign.com/fr/how-to-rotate-objects-in-inkscape/)

----

   <!--  - path: https://www.mu-inthecity.com/sites/default/files/articles/leonwuidar005.jpg
      absolute: true  
      caption: Léon Wuidar -->