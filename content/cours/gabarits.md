---
title: Gabarits et blocs de texte
slug: gabarits
disabled: false
path: cours10
thumb: thumb.jpg
active: false
ranking: 13
lang: fr-FR
module: edition
logiciel: Adobe Indesign
extrait: Première introduction à Adobe Indesign. Découverte de l'interface et des propriétés de document. Blocs de textes. ON évoquera aussi rapidement le travail de fin de quadri à venir...
content:
    - Indesign
    - gabarits
mediasgrid: 1
medias:
    - path: https://www.lespressesdureel.com/img_hd/img/ouvrage/9495/7.jpg
      absolute: true
      caption: Ghita Skali - Narrative Machines – Professional magazine of medical fictions
      link: https://www.lespressesdureel.com/ouvrage.php?id=9495&menu=0
      label:  
    - path: prose-du-transsiberien.jpeg
      absolute: false  
      caption: Prose du Transsibérien, Blaise Cendrars et Sonia Delaunay
      link: 
      label:
    - path: http://caso.baptiste-tosi.eu/content/cours-9/img/hp_4_lg.jpg
      absolute: true  
      caption: Pages de l'Hypnerotomachia Poliphili, impression par Alde Manuce, Venise, 1499.
      link: http://www.codex99.com/typography/82.html
      label: The Hypnerotomachia Poliphili
    - path: https://scontent.fbru5-1.fna.fbcdn.net/v/t39.30808-6/317488464_5658061730935975_4687613148917173003_n.jpg?_nc_cat=108&ccb=1-7&_nc_sid=730e14&_nc_ohc=D5kdRLmaDOgAX9cGMM_&tn=tRXgEj4c9_beKUby&_nc_ht=scontent.fbru5-1.fna&oh=00_AfC1WUpX4wxQPbcFbjKt9JseoOfpo96zkksr1GS4Nu2cBg&oe=638D856E
      absolute: true
      caption: Photobashing with Midjourney (from Sean Simon)
      link: https://www.lespressesdureel.com/ouvrage.php?id=9495&menu=0
      label:  

---

- **Réception des travaux bitmap**
- Retour sur les **filtrages** et **retouches** dans Krita
- Rappel **[Licenses Indesign](https://docs.google.com/forms/d/e/1FAIpQLSe9eTCnrifKvgu6SRvJhBoV58MpWNpGzE1QpPIzGXB--cyiYA/viewform?vc=0&c=0&w=1&flr=0)** (vous devez être connecté.e avec votre adresse de l'école)
- Point sur la fin de quadrimestre
- **Fold/Unfold**, organisation, premières lectures...
- Quelques exemples de [travaux rendus](https://drive.google.com/drive/folders/1hQM06jp13XR2ovBbub5ZsOSqlkTSg8Ux?usp=sharing)    

<!-- ### Les différentes étapes pour le travail de fin de quadri

+ Concept/idée, Maquette + penser la reliure ou l'assemblage. Quel type d'objet/livre voulez-vous créer? Quel type de structure/mise en page voulez-vous créer? En fonction de cela, construction d'une grille + choix des polices.
+ Rédaction/collection des contenus textes (Pensez à ce qui existe sur Internet / vérifiez les licenses)
+ Création des visuels (Krita, PSD..)
+ Création et intégration du dessin vectoriel -->

![](/cours/edition/gabarits/assemblage02.png)

## Alternatives

- [https://www.scribus.net/](https://www.scribus.net/)
- [La doc sur Floss Manuals FR](https://fr.flossmanuals.net/initiation-a-scribus/esquissez-votre-document/)
- [Affinity Publisher](https://affinity.serif.com/fr/publisher/)
- [AlternativeTo](https://alternativeto.net/)

<a data-fancybox title="" href="/assets/scribus01.jpg">![](/assets/scribus01.jpg)</a>

Les logiciels de mise en page sont le résultat d'une évolution technique et s'inscrivent dans une histoire millénaire. Vous en trouverez un aperçu sur [cette page](http://cours.baptiste-tosi.eu/doku.php?id=cours_10) éditée par Baptiste.

## Principe de l'assemblage

- [Préparer ses sources](https://fr.flossmanuals.net/scribus/preparer-les-sources/)
- **Assemblage**
- Export PDF simple

## Interface, Gabarits et pages

- Rapide tour de l'interface, et correspondances avec Inkscape ou Krita..
- Le panneau **Pages** ([Avec INDD](https://helpx.adobe.com/be_fr/indesign/using/pages-spreads-1.html)) + [planches-îlots](https://helpx.adobe.com/be_fr/indesign/using/pages-spreads-1.html) + multiples de 2
- Espace de travail
- Marges, colonnes
- **Créer des repères** (+ création sur gabarit)
- Principe des **gabarits**
- Les calques dans Indesign
- Numérotation automatique

## Le travail du texte dans Indesign

- **Blocs de texte** et colonnes + **tracés** à la plume
- Texte de substitution + [Liste de générateurs](https://loremipsum.io/fr/ultimate-list-of-lorem-ipsum-generators/)
- **Styles** de paragraphes et de caractères
- Le texte en excès
- **Chaînages** de texte
- **Alignements** (sur la sélection, la page, les marges..)
- Trouver et installer des **polices** (Attention à les intégrer au document par la suite)

<a data-fancybox title="Rythmes" href="/assets/rythme.jpg">![Olafur](/assets/rythme.jpg)</a>
[Source](https://www.nundesign.fr/fondamentaux-graphiques/grille-de-mise-en-page-principe-et-utilisation)

# Liens intéressants

+ Les [formats](https://papersizes.io/)
+ Les [nombreuses et splendides couvertures](http://cover.baptiste-tosi.eu/) que Baptiste a mis en ligne pour vous.
+ Sur les [grilles de mise en page](https://www.nundesign.fr/fondamentaux-graphiques/grille-de-mise-en-page-principe-et-utilisation)
+ Les [poemes de Carl Andre](https://www.google.com/search?q=carl+andre+poemes&sxsrf=ALeKk01DugxnP-7_8qW3R57oOlww_3vSdg:1607024642107&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjTgcCnybLtAhVRKuwKHTg-BHgQ_AUoAXoECAYQAw&biw=1199&bih=1338)
+ Quelques exemples de [grille modulaire](http://yharel.free.fr/data/informatique/logiciels/bureau/traitement_de_texte/typographie/mise_page_modulaire.htm)

------

### Où trouver des typographies libres de qualité?

+ [Font Library](https://fontlibrary.org/), un agrégateur de fontes sous licences libres
+ [VTF](http://velvetyne.fr/), une fonderie libre
+ [use&modify](http://usemodify.com/), une sélection de fontes libres par l'artiste [Raphaël Bastide](https://raphaelbastide.com/)
+ [Font Squirrel](https://www.fontsquirrel.com/), un autre agrégateur de fontes libres



<!-- # Pour la semaine prochaine: étape 1

+ Préparer votre **matériel photo** (min 6 images en haute définition - min 1800px de large) et textes (bio, légendes et descriptions des images, démarche..), pour ce travail.
+ **Avoir une idée relativement précise de ce que vous voulez réaliser. ça va aider..**. (Pensez à votre **[chemin de fer](https://www.google.com/search?sxsrf=ALeKk01be3ekh4n4jvSkIUeOXlrRlJMCxw:1607689139900&source=univ&tbm=isch&q=chemin+de+fer+mise+en+page&sa=X&ved=2ahUKEwjP9t7g9MXtAhURDOwKHWQyCEUQjJkEegQIBRAB&biw=1418&bih=1329#imgrc=XSZvCSDTVEBHvM))**
+ Faire donc une maquette reprenant vos grands principes de mise en page. Sur papier ou à l'aide du logiciel de votre choix.
+ Déposer vos images, textes et maquettes dans un dossier, dans notre dossier partagé, avant le prochain cours. Après, cela ne servira plus à rien. -->

----

