---
title: Images matricielles et sélections
slug: selections
mandatory: true
thumb: selections.jpg
caption: 
ranking: 1
lang: fr-FR
module: bitmap
cours: selections
extrait: Découverte de l'interface de Krita, des notions de résolution, définition et profondeur d'image, ainsi que des outils de sélections de base
mediasgrid: 1
medias:
    - path: magritte1.jpeg
      absolute: false  
      caption: René Magritte
    - path: magritte_2.jpg
      absolute: false  
    - path: magritte_resultat.jpg
      absolute: false  
      caption: René Magritte
gallery:
    - path: mag.jpg
      absolute:
      caption: Magritte dans Google images
---

### 1. Magritte (ou autre)

A partir du tableau de Magritte ci-contre (barre latérale), de [cet autre tableau](http://cours.baptiste-tosi.eu/lib/exe/fetch.php?media=22901741961_e619efbd78_b.jpg) (ou d'un autre qui convienne), séléctionnez un élément, une forme (le rideau, le personnage) pour remplacer ce qu'il contient par une autre image de votre choix. 

Le choix du bon outil de sélection ainsi que la qualité des images (de définition suffisante) seront ici très importants.

L'objectif de ceci est l'utilisation des outils de **sélection** et de **transformation**, ainsi que l'**utilisation des calques**.

Votre document final doit être un document de **800px sur 1000px minimum** (ou de 1000px sur 800px). A vous de trouvez des images de dimensions suffisantes. 

Le travail est à déposer dans votre dossier Drive personnel avnat le prochain cours.

<Gallery :imggallery="gallery" :imgsection="module" :imgcours="slug"></Gallery>

Vous trouverez ci-dessous une petite video d'intro à Krita de l'année dernière si besoin..

<iframe style="border-radius:8px;margin:1.5rem 0;" width="740" height="450" src="https://www.youtube.com/embed/XC6Ml3Py5tc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>