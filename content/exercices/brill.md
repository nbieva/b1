---
title: Josh Brill
slug: brill
mandatory: true
thumb: brill.jpg
caption: 
ranking: 7
lang: fr-FR
module: vectoriel
cours: plume
extrait: 
mediasgrid: 1
medias:
    - path: birds.jpg
      absolute: false  
      caption: Josh Brill
    - path: Black-cappedChickadee_JoshBrill.jpg
      absolute: false  
      caption: Josh Brill
    - path: brill2.png
      absolute: false  
      caption: Josh Brill
    - path: brill3.png
      absolute: false  
      caption: Josh Brill
    - path: brill4.png
      absolute: false  
      caption: Josh Brill
    - path: brill.png
      absolute: false  
      caption: Josh Brill
    
---

Recréez ce visuel de [Paul Rand](https://www.google.com/search?q=paul+rand&sxsrf=ALeKk02ol23K-JYiC10m8TqsfAm3kwn0Yg:1614668927513&source=lnms&tbm=isch&sa=X&ved=2ahUKEwiiheq-hpHvAhUD7aQKHeBjABMQ_AUoAXoECA8QAw&biw=1544&bih=1338) à l'aide des outils vus au cours. Seront particulièrement utiles ici:

+ L'**import d'image**
+ L'usage des **calques** et/ou des **objets** (très importants ici)
+ Les **formes simples**
+ Les **transformations**
+ Le **dessin à la plume** et/ou l'**outil noeud**.
+ Les **opérations sur les chemins** (division)
+ Beaucoup de **méthode** et de **précision**
+ **Cmd + S** (ou Ctrl + S) est votre ami

Comme d'habitude, le fichier est à me remettre sur le drive au format .svg

Nous avons travaillé ceci durant quelques années sur Illustrator. Vous trouverez ci-dessous une vidéo sans filet dans laquelle j'essaie de trouver le moyen de le réaliser dans Inkscape avec les mêmes contraintes. ce n'est bien entendu qu'une possibilité parmi d'autres..

<iframe style="border-radius:8px;margin:1.5rem 0;" width="740" height="450" src="https://www.youtube.com/embed/ysxZxDu6-js" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
