---
title: Motif Vuitton
slug: vuitton
mandatory: true
thumb: vuitton.jpg
caption: 
ranking: 8
lang: fr-FR
module: vectoriel
cours: motifs
extrait: 
mediasgrid: 1
medias:
    - path: vuitton.png
      absolute: false  
      caption: Le motif Vuitton
    - path: murakami.jpg
      absolute: false  
      caption: La version de Murakami
---

Recréez ce visuel de [Paul Rand](https://www.google.com/search?q=paul+rand&sxsrf=ALeKk02ol23K-JYiC10m8TqsfAm3kwn0Yg:1614668927513&source=lnms&tbm=isch&sa=X&ved=2ahUKEwiiheq-hpHvAhUD7aQKHeBjABMQ_AUoAXoECA8QAw&biw=1544&bih=1338) à l'aide des outils vus au cours. Seront particulièrement utiles ici:

+ L'**import d'image**
+ L'usage des **calques** et/ou des **objets**
+ Les **formes simples**
+ Les **transformations**
+ Le **dessin à la plume** et/ou l'**outil noeud**.
+ Les **alignements**
+ Les **opérations sur les chemins** (Union, différence, intersection, etc.)
+ Les **découpes**

Sauvez régulièrement votre fichier.

Comme d'habitude, le fichier est à me remettre sur le drive au format .svg


<iframe style="border-radius:8px;margin:1.5rem 0;" width="740" height="450" src="https://www.youtube.com/embed/u84y2vAJAdY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

