---
title: The shapes project
slug: shapes-project
mandatory: true
thumb: shapes-project.jpg
caption: 
ranking: 2
lang: fr-FR
module: vectoriel
cours: plume
extrait: The Shapes Project est un projet initié par McCollum en 2005. L’artiste a créé un système permettant de générer plus de 31 milliards de formes différentes, à partir des combinaisons de six groupes d’éléments types.
mediasgrid: 1
medias:
    - path: MCCOLLUM_Allan_Shapes_Spinoffs.jpg
      absolute: false  
      caption: The Shapes Project
    - path: CHD1142.jpg
      absolute: false  
      caption: The Shapes Project
    - path: shapes.jpg
      absolute: false  
      caption: The Shapes Project
gallery:
    - path: worksheet.jpeg
      absolute: false  
      caption: Worksheet / The Shapes Project
---


> I’m presently using my home computer to construct Adobe Illustrator ‘vector’*files that allow the shapes to be produced in many possible ways. The shapes can be printed graphically as silhouettes or outlines, in any size, color or texture, using all varieties of graphics software; or, the files can be used by rapid prototyping machines and computer-numerically-controlled (CNC) equipment—such as routers, laser and waterjet cutters—to build, carve, or cut the shapes from wood, plastic, metal, stone, and other materials. (Allan McCollum)

<iframe width="740" height="460" src="https://www.youtube.com/embed/OfV9gMeETS4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## A propos du projet
The Shapes Project est un projet initié par McCollum en 2005: l’artiste a créé un système permettant de générer plus de 31 milliards de formes différentes, à partir des combinaisons de six groupes d’éléments types. Chaque forme est destinée à être assignée à un individu. Le volume I contient tous les « patrons », tandis que le volume II comprend le mode d’emploi pour réaliser toutes les combinaisons possibles de ces éléments. Puisant sa méthode dans l’analyse des systèmes de production de masse, The Shapes Project présente un paradoxe: le souhait de l’artiste de produire une œuvre d’art à une échelle massive, en veillant en même temps à ce qu’aucun de ces objets, pourtant créés à partir d’un même moule, ne soit tout à fait identique.

[Source](http://www.micheledidier.com/index.php/fr/the-book-of-shapes.html)

[Plus d'infos sur le projet](McCollum_Shapes.pdf)

http://allanmccollum.net/allanmcnyc/christineburgin/pp_mccollum_mullican.html

<Gallery :imggallery="gallery" :imgsection="module" :imgcours="'plume'"></Gallery>