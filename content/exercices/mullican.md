---
title: Matt mullican & Ikko Tanaka
slug: mullican
mandatory: true
thumb: tanaka-thumb.jpg
caption: 
ranking: 0
lang: fr-FR
module: vectoriel
cours: formes
extrait: Introduction au cours de B1Lorem, ipsum dolor sit amet consectetur adipisicing elit. Illum ipsum saepe temporibus voluptatum expedita natus reprehenderit eius dolor veritatis! Dolorum exercitationem nostrum itaque doloribus ut
mediasgrid: 1
medias:
    - path: mullican1.jpg
      absolute: false  
      caption: Matt Mullican
    - path: tanaka.jpg
      absolute: false
      caption: Ikko Tanaka
     
---


### Pour ceci, vous aurez besoin entre autres de:

+ **Création et propriétés d'un document** (taille, grilles..)
+ **Import d'image** matricielle (voir vidéo ci-dessous)
+ Tracé de **formes simples**
+ **Guides** intelligents et/ou grille
+ Gestion des **calques** et des **objets**
+ Utilisation basique du **magnétisme**
+ **Fonds et contours** + **Couleurs**
+ Utilisation de **raccourcis**
+ **Sauvegarde** de documents SVG
+ **Transfert** dans votre dossier sur le Drive (voir vidéo ci-dessous)

Notez vos points de bloquages. On en reparle au début du prochain cours.
Bon travail!

## 1. Matt Mullican

1. Dans l'image ci-contre (Image 1, Matt Mullican), **choisissez 5 signes** que vous reproduisez à l’aide des formes simples vues au cours.

2. Les dimensions pour chaque pictogramme est de 100x100mm 

3. Transfert de votre fichier/vos fichiers SVG dans votre dossier personnel sur le drive (nommez vos fichiers comme suit **votrenom-mullican.svg**, ou quelque chose du genre..)

## 2. Ikko Tanaka

1. Reproduisez l'un des deux posters ci-contre (Image 2, Ikko Tanaka) à l’aide des outils vus au cours.

2. Dimensions libres (attention à ne pas confondre cm et mm) mais conserver les proportions des posters.

3. Transfert de votre fichier SVG dans votre dossier personnel sur le drive (nommez vos fichiers comme suit **votrenom-tanaka.svg**, ou quelque chose du genre..)

---------
### Import d'images matricielles et 2-3 rappels :

<iframe class="video" width="740" height="460" src="https://www.youtube.com/embed/wqBc0o7mtO4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Document multipages dans Inkscape 1.2 :

<iframe class="video" width="740" height="460" src="https://www.youtube.com/embed/b7RSZAGeUxQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Une autre petite sur les dossier partagés et les rendus :

<iframe class="video" width="740" height="460" src="https://www.youtube.com/embed/i0MV9xuEX28" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Quelques liens concernant Inkscape:

+ [L'essentiel d'Inkscape](https://www.lafabriqueduloch.org/wp-content/uploads/2019/03/Tuto1_Inkscape_MS56.pdf) (Tout ce dont nous avons besoin pour commencer..)
+ [Inkscape for Adobe Illustrator users](https://wiki.inkscape.org/wiki/index.php/Inkscape_for_Adobe_Illustrator_users)



---

## Matt Mullican  

Matt Mullican est un artiste californien dont l’œuvre se divise en deux grandes parties. D’un côté, des modèles cosmologiques, des mondes réinventés dans une logique post-conceptuelle, avec des systèmes de symboles et de signes empruntés ou créés, et de l’autre côté, un travail lié à l’hypnose \(bien que les deux facettes ne soient pas antithétiques comme l’a montré l’exposition de l’Institut d’art contemporain,\_12 BY 2, \_en 2010\). La première entité fait appel à des logos, schémas et à des notions fondamentales et symboliques que Matt Mullican met en scène dans des dessins, maquettes, cartes, vidéos – il sera d’ailleurs un pionnier de la réalité virtuelle.

<iframe class="video" width="740" height="460" src="https://www.youtube.com/embed/OXwIfAnV9yk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

_The IAC Mural, 15 June 2010_, gigantesque fresque couvrant le mur jouxtant l’IAC illustre ainsi les cinq niveaux de la cosmogonie de Mullican : au vert correspond l’essentiel, les éléments, la mort, au bleu le mystère de l’inconscient \(_the world unframed_\), au jaune les manifestations conscientes \(_the world framed_\), au noir, le langage et au rouge le spirituel, les idées, le paradis. L’autre aspect de son travail est inauguré par des performances théâtrales à partir des années 1970, dans lesquelles, hypnotisé, il se projette dans des images puis fait le récit de son expérience. Lui qui a passé son enfance à Rome commence avec une gravure de Piranèse, dans laquelle il se promène, passant au delà du visible. Il a de plus en plus recours à l’inconscient, utilisant des acteurs puis se mettant exclusivement en scène sous hypnose. C’est alors qu’apparaît malgré lui une autre personnalité. Tandis qu’il est hypnotisé, il réalise des œuvres, des calligraphies, des peintures, des écritures automatiques. Pour Mullican, l’auteur de ces œuvres est un autre accessible sous hypnose, ni un homme, ni une femme, un autre à l’identité multiple qu’il appelle toujours « That Person ».

En 2005, le musée Ludwig de Cologne commande ainsi une exposition à That Person et non à Matt Mullican. À l’IAC en 2010, c’est la rencontre des œuvres des deux qui est orchestrée dans les 12 salles comme le suggère le titre_12 BY 2_, l’exposition rassemblant 40 ans de création de Matt Mullican et de son double. Dans ces œuvres complexes, et sur un mode qui n’est pas sans rappeler celui des Surréalistes, Mullican travaille et joue en permanence avec notre perception du monde et de ses codes.

Source : [Institut d’art contemporain de Villeurbanne](http://i-ac.eu/fr/artistes/98_matt-mullican)

[Un lien audio](http://www.moma.org/explore/multimedia/audios/391/6832) où Matt Mullican explique la genèse de son travail \(Eng\)

[Un autre…](http://www.poptronics.fr/Matt-Mullican-portrait-de-l)
