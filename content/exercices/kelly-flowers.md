---
title: Kelly Flowers
slug: kelly-flowers
mandatory: true
thumb: kelly.jpg
caption: 
ranking: 5
lang: fr-FR
module: vectoriel
cours: plume
extrait: La passion de Kelly pour le contour, la silhouette, l’a poussé à dessiner presque continuellement. Le jardin des plantes à Paris, sous le châssis vitré de son atelier, est un de ses sujets favoris.
mediasgrid: 1
medias:
    - path: kelly1.jpeg
      absolute: false  
      caption: Elsworth Kelly, Flowers
---


La passion de Kelly pour le contour, la silhouette, l’a poussé à dessiner presque continuellement. Le jardin des plantes à Paris, sous le châssis vitré de son atelier, est un de ses sujets favoris. II dessine ces plantes inlassablement, avec une ligne forte et simple, parfaitement organique dans son rythme, mais austère. De temps en temps, l’image d’une peinture émerge de ces contours. Une image suggérée non pas par la feuille ou la tige, mais par l’espace.
[…]
Lors de l’exposition-hommage Matisse – Kelly au Musée national d’art moderne-Centre Georges Pompidou, le public a pu aprécier, notamment, la flamboyance des dessins de Kelly consacrés aux plantes. Camélia, Cyclamen, Mandarine, Poire, Orange, Branche de citron, Figue, Feuille de melon, Magnolia…Par leur élégance et leur simplicité ces lithographies originales nous démontrent que, bien que considéré comme un des artistes du XXème siècle les plus au faît de l’abstraction, Ellsworth Kelly sait retranscrire la réalité par le trait.

Source : maeght.com
