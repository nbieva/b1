---
title: Masques de filtrages
slug: filtrages
mandatory: true
thumb: filtrages.jpg
caption: 
disabled: true
ranking: 3
lang: fr-FR
module: bitmap
cours: filtrages
extrait:
mediasgrid: 1
medias:
    - path: https://jamesturrell.com/wp-content/uploads/2013/04/stonescape_outside_11-1440x2152.jpg
      absolute: true  
      caption: L'imagine originale
    - path: https://i.pinimg.com/originals/fc/0a/27/fc0a2701882e2d102fc9135776254795.jpg
      absolute: true  
      caption: James Turrell, “Stone Sky”, 2005, Stonescape, Napa Valley, California. 
    - path: base-eliasson.jpg
      absolute: false  
      caption: 
    - path: base-beecroft.jpg
      absolute: false  
      caption: 
    - path: base-resultat.jpg
      absolute: false  
      caption: 

---

En guise de dernier exercice Bitmap, vous pouvez choisir **un des deux** exercices repris ci-dessous. 

## 1. James Turrell

Créez en partant de l'image 1 trois variations de couleurs/lumières (comme sur l'image 2) en utilisant les masques de filtrages (vous pouvez en utiliser plusieurs).
Il faut donc au final me rendre 3 fichiers JPG différents, de la taille et du format de l'image 1.

Utilisez les réglages que vous voulez (le but c'est d'explorer) mais la balance de couleur et/ou l'ajustement TSV vous seront probablement utiles..

## 2. Olafur Eliasson et Vanessa Beecroft

Partez des images 3 et 4 pour produire le résultat de l'image 5. Le fichier au format Krita (kra) est à déposer dans votre dossier avant le prochain cours. Pour cet exercice, il existe cette petite vidéo pour vous aider.

<iframe width="740" height="460" src="https://www.youtube.com/embed/9N7owrYrE3Q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



<!-- ## Olafur Eliasson

En partant du fichier ci-contre ([alignements-source.svg](Image 4), réorganisez les éléments pour qu'ils correspondent au rendu final de l'image 5. Les différents éléments doivent être parfaitements alignés et orientés. Vérifiez votre document en mode contours (Affichage > Mode d'affichage > Contours) dès les premières opérations afin de ne pas dupliquer les erreurs éventuelles.

## Make-up

En partant du fichier ci-contre

## Parasols

En partant du fichier ci-con

## Turrell

A partir du fichier ci-contre (4), créez 3 variations plausibles de cet espace en modifiant la lumière, les couleurs, etc.. -->