---
title: Les masques
slug: masques
mandatory: true
thumb: masques.jpg
caption: 
ranking: 2
lang: fr-FR
module: bitmap
cours: masques
extrait: Découverte de l'interface de Krita, des notions de résolution, définition et profondeur d'image, ainsi que des outils de sélections de base
mediasgrid: 2
medias:
    - path: empiredeslumieres.jpg
      absolute: false  
      caption: René Magritte
    - path: empiredeslumieresnew.jpg
      absolute: false  
      caption: Une variation sur le même thème..
    - path: kanghee01.png
      absolute: false  
      caption: Kim KangHee
      fullwidth: true
    - path: resultat2.jpg
      absolute: false  
      caption: Le résultat à obtenir
    - path: le-poison.jpg
      absolute: false  
      caption: Le poison, René Magritte
    - path: coucher-de-soleil.jpg
      absolute: false  
      caption: Un coucher de soleil
    - path: nuage.jpg
      absolute: false  
      caption: Un nuage
---

Choisissez un des trois exercices ci-dessous. L'accent sera mis ici sur le **travail non destructif de l'image**, à l'aide des masques de transparence (masques de fusion dans Photoshop). Nous avons vu au cours qu'ils fonctionnent exactement de la même façon dans les différents logiciels.

+ Doivent être remis dans votre dossier sur le Drive, avant le prochain cours, votre fichier Krita (.kra) et un export JPG.
+ **Sauvez régulièrement votre travail!**

Vous pouvez vous baser sur la video ci-dessous. Les techniques sont valables pour les trois exercices.

<iframe width="740" height="400" src="https://www.youtube.com/embed/vNC4bQoSvuI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## 1. Empire des lumières

+ Partez de l'image de l'Empire des lumières ci-contre et remplacez le ciel par une autre image de votre choix (créez quelque chose d'intéressant...) en utilisant les masques de transparence et les outils vus au cours. 
+ La taille de votre document sera dans ce cas-ci, bien évidemment, la taille de l'image originale du tableau ci-contre (image 1).

## 2. Digital Collage

Composez une image de 2000 x 2000px.

La matière première de votre image doit être un minimum 10 autres images. Attention à toujours garder un oeil sur la définition de vos images sources.

+ Jouez sur l'échelle
+ Songez éventuellement à rester (en fonction du résultat souhaité) dans une même gamme de couleur et avec une luminosité similaire (en focntion de vos combinaisons)
+ Vous pouvez également expérimenter les différentes brosses par défaut de Krita.
+ Songez à toutes les images et sujets qui sont particulièrement adaptés au principe des masques (que vous pouvez facilement transformer en image en niveaux de gris)

**ATTENTION: Cette fois, travail non-destructif de l'image!** (D'où l'utilisation des masques..)
Et veillez à la bonne organisation de vos calques ainsi qu’à la qualité des images utilisées! La recherche d'images adaptées et de qualité est une des étapes les plus importantes de ce travail.
**Sauvez régulièrement votre travail!**


## 3. Le poison

Réalisez cette première image (numéro 4) sur base des trois suivantes (5, 6 et 7).