---
title: Paul Rand
slug: paul-rand
mandatory: true
thumb: paul-rand.jpg
caption: 
ranking: 4
lang: fr-FR
module: vectoriel
cours: plume
extrait: 
mediasgrid: 1
medias:
    - path: westing.jpg
      absolute: false  
      caption: Westinghouse annual report cover 1970, Paul Rand (Designer)
---

Recréez ce visuel de [Paul Rand](https://www.google.com/search?q=paul+rand&sxsrf=ALeKk02ol23K-JYiC10m8TqsfAm3kwn0Yg:1614668927513&source=lnms&tbm=isch&sa=X&ved=2ahUKEwiiheq-hpHvAhUD7aQKHeBjABMQ_AUoAXoECA8QAw&biw=1544&bih=1338) à l'aide des outils vus au cours. Seront particulièrement utiles ici:

+ L'**import d'image**
+ L'usage des **calques** et/ou des **objets**
+ Les **formes simples**
+ Les **transformations**
+ Le **dessin à la plume** et/ou l'**outil noeud**.
+ Les **alignements**
+ L'outil **pipette** (nous ne l'avons pas vu au cours, mais cela vaut la peine de l'essayer)
+ Les **opérations sur les chemins** (Union, différence, intersection, etc.)
+ Les **découpes**
+ les très motivé.e.s peuvent faire la signature ;-)

Sauvez régulièrement votre fichier.

Comme d'habitude, le fichier est à me remettre sur le drive au format .svg

Cette vidéo pourrait aider certain.e.s. 

<iframe style="border-radius:8px;margin:1.5rem 0;" width="740" height="450" src="https://www.youtube.com/embed/o7QcOk5pkBA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
