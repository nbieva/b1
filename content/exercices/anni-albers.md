---
title: Anni Albers
slug: anni-albers
mandatory: true
thumb: albers-thumb.jpg
caption: 
ranking: 1
lang: fr-FR
module: vectoriel
cours: alignements
extrait: Introduction au cours de B1Lorem, ipsum dolor sit amet consectetur adipisicing elit. Illum ipsum saepe temporibus voluptatum expedita natus reprehenderit eius dolor veritatis! Dolorum exercitationem nostrum itaque doloribus ut
mediasgrid: 1
medias:
    - path: https://textileartscenter.com/wp-content/uploads/2018/05/ANNI_2-916x1024.jpg
      absolute: true  
      caption: Anni Albers, Study for Camino Real, 1967. Gouache on paper. 44.5 x 40.6 cm. The Josef and Anni Albers Foundation, Bethany, Connecticut.
    - path: anni.png
      absolute: false
      caption: Design for Jacquard Weaving
    - path: https://www.du-grand-art.fr/wp-content/uploads/2020/12/motif-tissage-anni-albers.jpg
      absolute: true
      caption: Motif
    - path: alignements-source.svg
      absolute: false  
      caption: Téléchargez le fichier de base 
    - path: alignements.jpg
      absolute: false  
      caption: Réorganisez les rectangles pour obtenir le résultat ci-dessus

     
---

<iframe class="video" width="740" height="460" src="https://www.youtube.com/embed/8iy4trWILVo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Alignements (au cours)

En partant du fichier ci-contre ([alignements-source.svg](Image 4), réorganisez les éléments pour qu'ils correspondent au rendu final de l'image 5. Les différents éléments doivent être parfaitements alignés et orientés. Vérifiez votre document en mode contours (Affichage > Mode d'affichage > Contours) dès les premières opérations afin de ne pas dupliquer les erreurs éventuelles.

Votre travail doit être déposé dans votre dossier partagé sur le Drive à la fin du cours.
## Anni Albers

Sur base du travail d'[Anni Albers](https://www.1stdibs.com/introspective-magazine/anni-albers-touching-vision/) (ou d'un.e autre artiste de votre choix), **créez un ou plusieurs patterns sur base de formes géométriques parfaitement alignées et paramétrées**. Objectif de cet exercice: travail sur les alignements paramétrés et sur les groupes.
Vous pouvez bien sûr, pour ceci, vous aider d'une grille, qu'elle soit rectangulaire ou axonométrique, ou travailler avec les pavages (effets de chemin), les motifs, ou les pavages de clones.

+ **Format A4** (297x210mm, portrait ou paysage)

Votre travail doit être déposé dans votre dossier partagé sur le Drive avant le prochain cours.

+ [https://www.1stdibs.com/introspective-magazine/anni-albers-touching-vision/](https://www.1stdibs.com/introspective-magazine/anni-albers-touching-vision/)
+ [Sur Google images](https://www.google.be/search?q=anni+albers&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjuyNvSxtvdAhXELlAKHVE2CnwQ_AUICigB&biw=1920&bih=947)

---

## Anni Albers  

Face à la montée du nazisme, Anni et Josef Albers quittent l'Allemagne en 1933. Ils partent enseigner au Black Mountain College en Caroline du Nord. En 1934, Anni Albers y crée un l’atelier de tissage. À partir de 1949, elle intervient dans de nombreuses écoles d’art comme spécialiste de l’art textile. La même année, le Museum of Modern Art (MOMA) lui consacre une exposition personnelle. Cela marque la reconnaissance de l'artiste et de l'art textile5.

Elle étudie les tissages traditionnels sud-américains et s'intéresse particulièrement à l'art textile péruvien qui servait de moyen de communication1. En 1965, elle réalise Six prayers pour le Musée juif de New York à la mémoire des victimes de la Shoah : six panneaux sombres et contemplatifs représentent les six millions de victimes juives1.

Abandonnant ensuite l'activité physiquement éprouvante du tissage, elle se consacre à la gravure et expérimente plusieurs techniques d’impression1. En 1975, son travail est exposé au Kunstmuseum de Düsseldorf, et au Bauhaus-Archiv, à Berlin4, à la Tate Modern de Londres en 2018-20196.

Elle publie également deux livres importants : une courte anthologie d'essais intitulée On Designing en 1959 et un livre fondateur en 1965, On Weaving, sorte d'atlas visuel explorant 4 000 ans d'histoire du tissage dans le monde et décrivant les techniques employées1.

Elle inspire des générations d'artistes. Sheila Hicks découvre l'art textile dans son atelier dans les années 19502. 

