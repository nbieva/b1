---
title: Ikko Tanaka
slug: tanaka
mandatory: true
thumb: tanaka-thumb.jpg
caption: 
ranking: 1
lang: fr-FR
module: vectoriel
cours: alignements
extrait: Introduction au cours de B1Lorem, ipsum dolor sit amet consectetur adipisicing elit. Illum ipsum saepe temporibus voluptatum expedita natus reprehenderit eius dolor veritatis! Dolorum exercitationem nostrum itaque doloribus ut
mediasgrid: 1
medias:
    - path: https://i.pinimg.com/originals/25/62/f3/2562f369c0a2d7ef2193b93aec115b5b.jpg
      absolute: true  
      caption: Idea 44, 1960
    - path: http://indexgrafik.fr/wp-content/uploads/Ikko-Tanaka-graphisme-japon-affiches-00.jpg
      absolute: true
      caption: 
      link: http://indexgrafik.fr/ikko-tanaka/
      label: Plus d'infos
     
---

Créez un document de 594 x 840px et placez une grille rectangulaire afin de réaliser l'affiche de gauche dans le visuel ci-dessous. Sauvegardez au format SVG et déposez le fichier dans notre dossier partagé.

Vous aurez besoin pour ceci de :

+ **Configuration du document + grilles**
+ **Import d'image**
+ Gestion des **calques/objets** (empilement, verrouillage..)
+ **Formes** simples + manipulations simples de **chemins**.
+ **Transformations** simples + contraintes (+Ctrl)
+ Gestion du **magnétisme**
+ **Couleur** (pipette)
+ **Dégradés** de couleurs
+ Sauvergarde et **export** de document.

Vous pouvez ensuite tenter celle de droite..

<a data-fancybox="simples" title="" href="/assets/galeries/simples/Ikko-Tanaka.jpeg">
    <img src="/assets/galeries/simples/Ikko-Tanaka.jpeg">
</a>

+ [Ikko Tanaka](https://designreviewed.com/artefacts/idea-44-1960/)