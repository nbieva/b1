---
title: Styles
slug: styles
mandatory: true
thumb: thumb.jpg
caption: 
ranking: 1
lang: fr-FR
module: edition
cours: styles
extrait: Introduction au cours de B1Lorem, ipsum dolor sit amet consectetur adipisicing elit. Illum ipsum saepe temporibus voluptatum expedita natus reprehenderit eius dolor veritatis! Dolorum exercitationem nostrum itaque doloribus ut
mediasgrid: 1
medias:
---

Recréez ce visuel de [Paul Rand](https://www.google.com/search?q=paul+rand&sxsrf=ALeKk02ol23K-JYiC10m8TqsfAm3kwn0Yg:1614668927513&source=lnms&tbm=isch&sa=X&ved=2ahUKEwiiheq-hpHvAhUD7aQKHeBjABMQ_AUoAXoECA8QAw&biw=1544&bih=1338) à l'aide des outils vus au cours. Seront particulièrement utiles ici:

+ L'**import d'image**
+ L'usage des **calques** et/ou des **objets**
+ Les **formes simples**
+ Les **transformations**
+ Le **dessin à la plume** et/ou l'**outil noeud**.
+ Les **alignements**
+ L'outil **pipette** (nous ne l'avons pas vu au cours, mais cela vaut la peine de l'essayer)
+ Les **opérations sur les chemins** (Union, différence, intersection, etc.)
+ Les **découpes**
+ les très motivé.e.s peuvent faire la signature ;-)

Sauvez régulièrement votre fichier.

Comme d'habitude, le fichier est à me remettre sur le drive avant le prochain cours au format .svg

Cette vidéo pourrait aider certain.e.s. 

<iframe style="border-radius:8px;margin:1.5rem 0;" width="740" height="450" src="https://www.youtube.com/embed/o7QcOk5pkBA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
