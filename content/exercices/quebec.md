---
title: Québec
slug: quebec
mandatory: true
thumb: quebec.jpg
caption: 
ranking: 3
lang: fr-FR
module: vectoriel
cours: plume
extrait: Introduction au cours de B1Lorem, ipsum dolor sit amet consectetur adipisicing elit. Illum ipsum saepe temporibus voluptatum expedita natus reprehenderit eius dolor veritatis! Dolorum exercitationem nostrum itaque doloribus ut
mediasgrid: 1
medias:
    - path: canada.png
      absolute: false  
      caption: Drapeau canadien
    - path: 750px-Fleur_de_lys_du_quebec.png
      absolute: false  
      caption: Fleur de Lys
    - path: quebec.png
      absolute: false  
      caption: Drapeau québéquois
---

+ Dessiner la Fleur de Lys sur base du modèle ci-contre. Une fois terminée, votre fleur de Lys doit être un seul tracé fermé (vérifiez dans votre panneau objets).
+ Utilisez-la ensuite pour reproduire le drapeau avec les rectangles bleus. Vous aurez pour cela besoin des alignements et des groupes que nous avons vu au deuxième cours.

La vidéo ci-dessous devrait vous aider.

<iframe style="margin-top:2rem;" width="740" height="460" src="https://www.youtube.com/embed/HUjlwZt-GA4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<a data-fancybox title="Fleur de lys" href="/assets/750px-Fleur_de_lys_du_quebec.png">![Fleur de lys](/assets/750px-Fleur_de_lys_du_quebec.png)</a>
<a data-fancybox title="Québec flag" href="/assets/quebec.png">![Québec flag](/assets/quebec.png)</a>