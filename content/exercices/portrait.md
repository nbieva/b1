---
title: Portrait
slug: portrait
sluggallery: plume
mandatory: true
thumb: portrait.jpg
caption: 
ranking: 6
lang: fr-FR
module: vectoriel
cours: plume
extrait: Introduction au cours de B1Lorem, ipsum dolor sit amet consectetur adipisicing elit. Illum ipsum saepe temporibus voluptatum expedita natus reprehenderit eius dolor veritatis! Dolorum exercitationem nostrum itaque doloribus ut
mediasgrid: 1
medias:
    - path: http://indexgrafik.fr/wp-content/uploads/Ikko-Tanaka-graphisme-japon-detail-affiche-shiseido.jpg
      absolute: true  
      caption: Ikko Tanaka
    - path: https://www.popwebdesign.net/popart_blog/wp-content/uploads/2018/04/pop-art-meets-op-art-malika-favre-757.jpg
      absolute: true  
      caption: Malika Favre
    - path: Alex-Katz_Brisk-Day.jpg
      absolute: false  
      caption: Alex Katz, Brisk Day, 1990 (Xylogravure / Aquatinte / Lithographie)
      link: https://www.google.be/search?q=alex+katz&rlz=1C5CHFA_enBE752BE753&source=lnms&tbm=isch&sa=X&ved=0ahUKEwitor7yy_zYAhWMyKQKHd-vAbsQ_AUICigB&biw=1307&bih=709#imgrc=_
      label: Lien
gallery:
    - path: lubna.svg
      absolute: false  
      caption: Lubna
    - path: melanie.jpg
      absolute: false  
      caption: Mélanie
    - path: maguelone.svg
      absolute: false  
      caption: Maguelone
    - path: coline.svg
      absolute: false  
      caption: Coline
---


Créez, dans le format de votre choix, **votre autoportrait (ou le portrait d'une autre personne de votre choix) à l'aide de la plume** et des autres outils vus au cours. 

Partir d'une photo sera évidemment ici d'une grande aide. Cela pourrait également vous aider de faire une première étude rapide de formes sur papier à l'aide de votre crayon préféré. 

Une fois les principales lignes dessinées, vous pouvez prendre ce dessin en photo ou le scanner pour l'importer ensuite dans Inkscape comme nous l'avons fait avec d'autres images. Vous pourrez alors vous en servir comme modèle.

Essayez d'apporter un soin particulier au choix de vos couleurs pour ceci.

Transferez votre fichier vectoriel dans votre dossier sur le Drive (nommez vos fichiers comme suit **votrenom-portrait.svg**, par exemple)

### Quelques inspirations:

+ [Le travail de Malika Favre](https://www.google.com/search?q=malika+favre&sxsrf=ALeKk03dmqmwnCxzGWMSmpelFbEXuPmo3g:1613808737886&source=lnms&tbm=isch&sa=X&ved=2ahUKEwi967mEgvjuAhVWNewKHRVJCOQQ_AUoAXoECA0QAw&biw=1193&bih=462)
+ [Le travail d'Alex Katz](https://www.google.be/search?q=alex+katz&rlz=1C5CHFA_enBE752BE753&source=lnms&tbm=isch&sa=X&ved=0ahUKEwitor7yy_zYAhWMyKQKHd-vAbsQ_AUICigB&biw=1307&bih=709#imgrc=EK0SP3netCLObM)

<Gallery :imggallery="gallery" :imgsection="module" :imgcours="sluggallery"></Gallery>