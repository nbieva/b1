---
title: Assemblages et exports
slug: exports
disabled: true
path: exports
thumb: thumb.jpg
active: false
ranking: 15
lang: fr-FR
module: edition
logiciel: Adobe Indesign
extrait: Lorem, ipsum dolor sit amet consectetur adipisicing elit. Illum ipsum saepe temporibus voluptatum expedita natus reprehenderit eius dolor veritatis.
content:
    - Indesign
    - gabarits
mediasgrid: 1
medias:  
exercice: 
    slug: exports
    label: Exports
    thumb: 
    rank: 1
    horscours: true
---

Contenu du cours