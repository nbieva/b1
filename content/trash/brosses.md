---
title: Les brosses (B)
longtitle: Le moteur de brosses de Krita
slug: brosses
disabled: false
path: brosses
thumb: hartung.jpg
caption: 
active: false
current: false
ranking: 9
lang: fr-FR
module: bitmap
logiciel: Krita
extrait: Découverte du moteur de brosses de Krita et de ce qu'est, fondamentalement, une brosse. Nous ferons le lien avec les workshops entre autres.. Création et modifications de brosses.
content:
    - Masques de transparence
mediasgrid: 1
medias:
    - path: https://artlogic-res.cloudinary.com/w_1000,h_1000,c_limit,f_auto,fl_lossy,q_auto/ws-galerieboulakia/usr/images/artists/artist_image/items/f2/f26a30579100497bb831c41fc7060dc3/hans_hartung_14.jpg
      absolute: true
      caption: Hans Hartung
    - path: brushes1@2x.jpg
      absolute: false  
      caption: Krita 4 Preset Bundle Overview
      link: https://docs.krita.org/en/reference_manual/krita_4_preset_bundle.html
---

+ Mode **interface cachée** (Tab)
+ **Superposition** d'images + Modes de fusion
+ [Les préréglages de brosses](https://docs.krita.org/en/reference_manual/krita_4_preset_bundle.html)
+ Modifier la taille de la brosse à la volée: Shift + Clic + Aller vers la droite ou vers la gauche
+ Pipette rapide : Cmd + Clic
+ Tab : **Interface cachée**
+ Mode **plein écran** : Clic droit puis le bouton correspondant (+- en bas à gauche du module)
+ **Opacité**: I + O
+ **Luminosité**: K + L
+ Rotation de l'espace de travail
