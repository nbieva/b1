---
title: Introduction
slug: intro
thumb: gilbert.jpg
active: false
ranking: 0
lang: fr-FR
extrait: Le cours d'Arts numériques B1 vous introduira à deux principaux types d'images numériques, les images vectorielles (opens new window) et les images bitmap (opens new window) ainsi qu'à quelques bases de mise en page. Il est complémentaire aux plénières, qui mettront tout cela en perspective, et au workshop qui nous introduira à l'art du code.
---

## Calendrier

![Calendrier](calendrier2022-2023.jpg)

### Module vectoriel

+ **23 septembre** > Cours 1 - Vectoriel 1 : Formes simples
+ **30 septembre** > Cours 2 - Vectoriel 2 : Groupes et alignements
+ **7 octobre** > Cours 3 - Vectoriel 3 : Plume **+ présentation Cartographie**
+ **10 octobre** > workshop code (vérifiez si cela vous concerne..)
+ **14 octobre** > Cours 4 - Vectoriel 4 : Pathfinder et motifs
+ **17 octobre** > workshop code (vérifiez si cela vous concerne..)

------

### Module bitmap

+ **21 octobre** > Cours 5 - Bitmap 1 : Introduction et sélections **+ Remise cartographie**
+ **28 octobre** > Cours 6 - Bitmap 2 : Masques de transparence
+ (congé d'automne)
+ **18 novembre** > Cours 7 - Bitmap 3 : Masques de filtrage/couleur **+ présentation Carte postale**
+ **25 novembre** > Cours 8 - Bitmap 4 : Masques de filtrage/couleur

------

### Module mise en page

+ **2 décembre** > Cours 9 - Mise en page 1 : Gabarits et blocs de texte
+ **9 décembre** > Cours 10 - Mise en page 2 : Images
+ **16 décembre** > Cours 11 - Mise en page 3 : Exports et assemblage + atelier **+ présentation Fold/Unfold**
+ **23 décembre** > Cours 12 - Mise en page 4 : Atelier et lectures de travaux

## Evaluation

L'évaluation de ce cours et une évaluation continue. Outre une présence régulière, des exercices hebdomadaires vous seront demandés (chaque fois pour le cours suivant) ainsi que des travaux artistiques personnels à la fin de chaque module. Les briefings et informations à propos de ces travaux artistiques seront disponibles sur digitalab.be la plateforme générale du CASO (lien en haut à droite).

## Rendus en ligne

Les rendus des exercices se font exclusivement via le Drive auquel vous avez accès via votre adresse @lacambre.be. Un dossier personnel sera créé pour chaque étudiant.e. Ce dossier, partagé avec moi, sera l'espace où les différents travaux seront rendus. Aucun rendu, hors de ce dossier, ne sera évalué.

Nommez vos fichiers de façon explicite afin qu'ils ne s'égarent pas. (nom-prenom-atelier-nomdelexercice.svg, par exemple)

Dans tous les cas, vous êtes responsables de vos fichiers et devez toujours en garder une copie locale.

<iframe class="video" width="740" height="460" src="https://www.youtube.com/embed/i0MV9xuEX28" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>